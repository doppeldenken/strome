# Strome

BitTorrent CLI

[![pipeline status](https://gitlab.com/doppeldenken/strome/badges/main/pipeline.svg)](https://gitlab.com/doppeldenken/strome/-/commits/main) [![coverage report](https://gitlab.com/doppeldenken/strome/badges/main/coverage.svg)](https://gitlab.com/doppeldenken/strome/-/commits/main) [![Latest Release](https://gitlab.com/doppeldenken/strome/-/badges/release.svg)](https://gitlab.com/doppeldenken/strome/-/releases)

&nbsp;

## Table of Contents

- [Installation](#installation)
- [Usage](#usage)

&nbsp;

## Installation

How can you install me?

&nbsp;

## Usage

How can you use me?
