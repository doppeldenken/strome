package observability

import (
	"log"
	"os"
)

//nolint
var stromeLogger *StromeLogger

const (
	NoLogsLevel   = 0
	InfoLogLevel  = 1
	DebugLogLevel = 2
	TraceLogLevel = 3
)

type StromeLogger struct {
	debugLogger *log.Logger
	infoLogger  *log.Logger
	logLevel    int
	traceLogger *log.Logger
}

type IStromeLogger interface {
	Log(info string)
	SetLogLevel(logLevel int)
}

func (stromeLogger *StromeLogger) Log(info string, logLevel int) {
	if stromeLogger.logLevel == NoLogsLevel {
		return
	}

	if logLevel != InfoLogLevel && logLevel != DebugLogLevel && logLevel != TraceLogLevel {
		logLevel = InfoLogLevel
	}

	if stromeLogger.logLevel < logLevel {
		return
	}

	switch logLevel {
	case InfoLogLevel:
		stromeLogger.infoLogger.Println(info)
	case DebugLogLevel:
		stromeLogger.debugLogger.Println(info)
	case TraceLogLevel:
		stromeLogger.traceLogger.Println(info)
	}
}

func (stromeLogger *StromeLogger) SetLogLevel(logLevel int) {
	stromeLogger.logLevel = logLevel
}

func GetLogger(logLevel int) *StromeLogger {
	if stromeLogger == nil {
		stromeLogger = newLogger(logLevel)
	}

	return stromeLogger
}

func newLogger(logLevel int) *StromeLogger {
	if logLevel != InfoLogLevel && logLevel != DebugLogLevel && logLevel != TraceLogLevel {
		logLevel = InfoLogLevel
	}

	logger := StromeLogger{
		logLevel: logLevel,
	}

	logger.infoLogger = log.New(os.Stdout, "INFO: ", log.Ldate|log.Ltime)
	logger.debugLogger = log.New(os.Stdout, "DEBUG: ", log.Ldate|log.Ltime)
	logger.traceLogger = log.New(os.Stdout, "TRACE: ", log.Ldate|log.Ltime)

	return &logger
}
