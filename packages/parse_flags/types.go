package flags

type Flags struct {
	Filepath string
	LogLevel int
	Version  bool
}
