package flags

const filepathFlagLongName = "filepath"

const filepathFlagShortName = "f"

const logLevelFlagLongName = "loglevel"

const logLevelFlagShortName = "l"

const versionFlagLongName = "version"

const versionFlagShortName = "v"

const flagsUsage = `strome usage:

--filepath, -f
	A description

--loglevel, -l
	A description

--version, -v
	Prints version


--help, -h
	Prints usage

`
