package flags

//nolint
import (
	"doppeldenken/strome/packages/observability"

	"flag"
	"fmt"
)

func Get() (flags Flags, err error) {
	setFlagUsage()

	filepath := parseFilepathFlag()
	logLevel := parseLogLevelFlag()
	version := parseVersionFlag()

	flag.Parse()

	if *logLevel != observability.InfoLogLevel &&
		*logLevel != observability.DebugLogLevel &&
		*logLevel != observability.TraceLogLevel {
		*logLevel = observability.InfoLogLevel
	}

	cliFlags := Flags{
		Filepath: *filepath,
		LogLevel: *logLevel,
		Version:  *version,
	}

	if cliFlags.Filepath == "" && cliFlags.Version == false {
		return cliFlags, NoFileSpecifiedError{}
	}

	return cliFlags, nil
}

func parseFilepathFlag() (flag *string) {
	var fileFlag string

	parseStringFlag(
		&fileFlag,
		filepathFlagLongName,
		filepathFlagShortName,
		"",
	)

	return &fileFlag
}

func parseLogLevelFlag() (flag *int) {
	var fileFlag int

	parseIntFlag(
		&fileFlag,
		logLevelFlagLongName,
		logLevelFlagShortName,
		1,
	)

	return &fileFlag
}

func parseVersionFlag() (flag *bool) {
	var versionFlag bool

	parseBoolFlag(
		&versionFlag,
		versionFlagLongName,
		versionFlagShortName,
		false,
	)

	return &versionFlag
}

func parseBoolFlag(
	flagValue *bool,
	longFlag,
	shortFlag string,
	defaultValue bool,
) {
	flag.BoolVar(
		flagValue,
		longFlag,
		defaultValue,
		"",
	)
	flag.BoolVar(
		flagValue,
		shortFlag,
		defaultValue,
		"",
	)
}

func parseIntFlag(
	flagValue *int,
	longFlag,
	shortFlag string,
	defaultValue int,
) {
	flag.IntVar(
		flagValue,
		longFlag,
		defaultValue,
		"",
	)
	flag.IntVar(
		flagValue,
		shortFlag,
		defaultValue,
		"",
	)
}

func parseStringFlag(
	flagValue *string,
	longFlag,
	shortFlag,
	defaultValue string,
) {
	flag.StringVar(
		flagValue,
		longFlag,
		defaultValue,
		"",
	)
	flag.StringVar(
		flagValue,
		shortFlag,
		defaultValue,
		"",
	)
}

func setFlagUsage() {
	//nolint:forbidigo
	flag.Usage = func() {
		fmt.Print(flagsUsage)
	}
}
