package metadata

type ITorrentMetadataInfo interface {
	CalculateFilePieceLimits() (filePieceLimits FilePieceLimits)
	GetLastPieceSize() int
	GetPiecesSha1Hashes() (hashes PiecesHashes)
	GetTorrentSha1Hash() (hash Sha1Hash, err error)
}

type TorrentMetadataInfo struct {
	Files       []TorrentMetadataInfoFile `bencode:"files"`
	Length      int                       `bencode:"length"`
	Name        string                    `bencode:"name"`
	PieceLength int                       `bencode:"piece length"`
	Pieces      string                    `bencode:"pieces"`
}

type oneFileHash struct {
	Length      int    `bencode:"length"`
	Name        string `bencode:"name"`
	PieceLength int    `bencode:"piece length"`
	Pieces      string `bencode:"pieces"`
}

type multipleFilesHash struct {
	Files       []TorrentMetadataInfoFile `bencode:"files"`
	Name        string                    `bencode:"name"`
	PieceLength int                       `bencode:"piece length"`
	Pieces      string                    `bencode:"pieces"`
}
