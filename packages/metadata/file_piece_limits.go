package metadata

type PieceIndex = int

type FileOffset = int

type FilePieceLimit struct {
	EndPiece         int
	EndPieceOffset   int
	Pieces           map[PieceIndex]FileOffset
	StartPiece       int
	StartPieceOffset int
}

type FilePieceLimits map[string]FilePieceLimit
