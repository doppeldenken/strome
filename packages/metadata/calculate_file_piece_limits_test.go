package metadata_test

//nolint
import (
	"doppeldenken/strome/packages/metadata"

	"testing"
)

func TestCalculateFilePieceLimits(t *testing.T) {
	t.Parallel()

	testCases := getCalculateFilePieceLimitsTestCases()

	for _, testCase := range testCases {
		testCase := testCase

		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()

			filePieceLimits := testCase.torrentMetadataInfo.CalculateFilePieceLimits()

			if !metadata.CompareFilePieceLimits(filePieceLimits, testCase.expectedFilePieceLimits) {
				t.Errorf(
					"got file piece limits '%v', expected file piece limits '%v'",
					filePieceLimits,
					testCase.expectedFilePieceLimits,
				)
			}
		})
	}
}

type calculateFilePieceLimitsTestCase struct {
	name                    string
	torrentMetadataInfo     metadata.TorrentMetadataInfo
	expectedFilePieceLimits metadata.FilePieceLimits
}

func getCalculateFilePieceLimitsTestCases() []calculateFilePieceLimitsTestCase {
	return []calculateFilePieceLimitsTestCase{
		{
			name: "successfully gets file piece limits (4 files, 15 length total, piece length == 5)",
			torrentMetadataInfo: metadata.TorrentMetadataInfo{
				Files: []metadata.TorrentMetadataInfoFile{
					{
						Path:   []string{"file1"},
						Length: 3,
					},
					{
						Path:   []string{"file2"},
						Length: 3,
					},
					{
						Path:   []string{"file3"},
						Length: 6,
					},
					{
						Path:   []string{"file4"},
						Length: 3,
					},
				},
				Length:      15,
				Name:        "test",
				PieceLength: 5,
				Pieces:      "123456789",
			},
			expectedFilePieceLimits: metadata.FilePieceLimits{
				"file1": metadata.FilePieceLimit{
					EndPiece:       0,
					EndPieceOffset: 3,
					Pieces: map[metadata.PieceIndex]metadata.FileOffset{
						0: 0,
					},
					StartPiece:       0,
					StartPieceOffset: 0,
				},
				"file2": metadata.FilePieceLimit{
					EndPiece:       1,
					EndPieceOffset: 1,
					Pieces: map[metadata.PieceIndex]metadata.FileOffset{
						0: 0,
						1: 2,
					},
					StartPiece:       0,
					StartPieceOffset: 3,
				},
				"file3": metadata.FilePieceLimit{
					EndPiece:       2,
					EndPieceOffset: 2,
					Pieces: map[metadata.PieceIndex]metadata.FileOffset{
						1: 0,
						2: 4,
					},
					StartPiece:       1,
					StartPieceOffset: 1,
				},
				"file4": metadata.FilePieceLimit{
					EndPiece:       2,
					EndPieceOffset: 4,
					Pieces: map[metadata.PieceIndex]metadata.FileOffset{
						2: 0,
					},
					StartPiece:       2,
					StartPieceOffset: 2,
				},
			},
		},
		{
			name: "successfully gets file piece limits (4 files, 15 length total, last piece bigger than needed, piece length == 4)",
			torrentMetadataInfo: metadata.TorrentMetadataInfo{
				Files: []metadata.TorrentMetadataInfoFile{
					{
						Path:   []string{"file1"},
						Length: 3,
					},
					{
						Path:   []string{"file2"},
						Length: 3,
					},
					{
						Path:   []string{"file3"},
						Length: 6,
					},
					{
						Path:   []string{"file4"},
						Length: 3,
					},
				},
				Length:      15,
				Name:        "test",
				PieceLength: 4,
				Pieces:      "123456789",
			},
			expectedFilePieceLimits: metadata.FilePieceLimits{
				"file1": metadata.FilePieceLimit{
					EndPiece:       0,
					EndPieceOffset: 3,
					Pieces: map[metadata.PieceIndex]metadata.FileOffset{
						0: 0,
					},
					StartPiece:       0,
					StartPieceOffset: 0,
				},
				"file2": metadata.FilePieceLimit{
					EndPiece:       1,
					EndPieceOffset: 2,
					Pieces: map[metadata.PieceIndex]metadata.FileOffset{
						0: 0,
						1: 1,
					},
					StartPiece:       0,
					StartPieceOffset: 3,
				},
				"file3": metadata.FilePieceLimit{
					EndPiece:       2,
					EndPieceOffset: 5,
					Pieces: map[metadata.PieceIndex]metadata.FileOffset{
						1: 0,
						2: 2,
					},
					StartPiece:       1,
					StartPieceOffset: 2,
				},
				"file4": metadata.FilePieceLimit{
					EndPiece:       3,
					EndPieceOffset: 3,
					Pieces: map[metadata.PieceIndex]metadata.FileOffset{
						3: 0,
					},
					StartPiece:       3,
					StartPieceOffset: 0,
				},
			},
		},
		{
			name: "successfully gets file piece limits (4 files, 15 length total, last piece bigger than needed, piece length == 2)",
			torrentMetadataInfo: metadata.TorrentMetadataInfo{
				Files: []metadata.TorrentMetadataInfoFile{
					{
						Path:   []string{"file1"},
						Length: 3,
					},
					{
						Path:   []string{"file2"},
						Length: 3,
					},
					{
						Path:   []string{"file3"},
						Length: 6,
					},
					{
						Path:   []string{"file4"},
						Length: 3,
					},
				},
				Length:      15,
				Name:        "test",
				PieceLength: 2,
				Pieces:      "123456789",
			},
			expectedFilePieceLimits: metadata.FilePieceLimits{
				"file1": metadata.FilePieceLimit{
					EndPiece:       1,
					EndPieceOffset: 1,
					Pieces: map[metadata.PieceIndex]metadata.FileOffset{
						0: 0,
						1: 2,
					},
					StartPiece:       0,
					StartPieceOffset: 0,
				},
				"file2": metadata.FilePieceLimit{
					EndPiece:       2,
					EndPieceOffset: 3,
					Pieces: map[metadata.PieceIndex]metadata.FileOffset{
						1: 0,
						2: 1,
					},
					StartPiece:       1,
					StartPieceOffset: 1,
				},
				"file3": metadata.FilePieceLimit{
					EndPiece:       5,
					EndPieceOffset: 3,
					Pieces: map[metadata.PieceIndex]metadata.FileOffset{
						3: 0,
						4: 2,
						5: 4,
					},
					StartPiece:       3,
					StartPieceOffset: 0,
				},
				"file4": metadata.FilePieceLimit{
					EndPiece:       7,
					EndPieceOffset: 1,
					Pieces: map[metadata.PieceIndex]metadata.FileOffset{
						6: 0,
						7: 2,
					},
					StartPiece:       6,
					StartPieceOffset: 0,
				},
			},
		},
	}
}
