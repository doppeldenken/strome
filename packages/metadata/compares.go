package metadata

//nolint
import (
	"doppeldenken/strome/packages/utils"
)

func ComparePiecesHashes(hashes1, hashes2 PiecesHashes) bool {
	if len(hashes1) != len(hashes2) {
		return false
	}

	for i := 0; i < len(hashes1); i++ {
		if !utils.Compare20ByteArray(hashes1[i], hashes2[i]) {
			return false
		}
	}

	return true
}

func CompareTorrentMetadata(metadata1, metadata2 TorrentMetadata) bool {
	switch {
	case metadata1.Announce != metadata2.Announce:
		return false
	case metadata1.Comment != metadata2.Comment:
		return false
	case metadata1.CreationDate != metadata2.CreationDate:
		return false
	case !CompareTorrentMetadataInfo(metadata1.Info, metadata2.Info):
		return false
	default:
		return true
	}
}

func CompareTorrentMetadataInfo(info1, info2 TorrentMetadataInfo) bool {
	switch {
	case info1.Length != info2.Length:
		return false
	case info1.Name != info2.Name:
		return false
	case info1.PieceLength != info2.PieceLength:
		return false
	case info1.Pieces != info2.Pieces:
		return false
	case !CompareTorrentMetadataInfoFiles(info1.Files, info2.Files):
		return false
	default:
		return true
	}
}

func CompareTorrentMetadataInfoFiles(files1, files2 []TorrentMetadataInfoFile) bool {
	if len(files1) != len(files2) {
		return false
	}

	for i := 0; i < len(files1); i++ {
		switch {
		case files1[i].Length != files2[i].Length:
			return false
		case !utils.CompareStringSlices(files1[i].Path, files2[i].Path):
			return false
		default:
			break
		}
	}

	return true
}

func CompareFilePieceLimits(filePieceLimits1, filePieceLimits2 FilePieceLimits) bool {
	if len(filePieceLimits1) != len(filePieceLimits2) {
		return false
	}

	for key, _ := range filePieceLimits1 {
		if !CompareFilePieceLimit(filePieceLimits1[key], filePieceLimits2[key]) {
			return false
		}
	}

	return true
}

func CompareFilePieceLimit(filePieceLimit1, filePieceLimit2 FilePieceLimit) bool {
	if filePieceLimit1.StartPiece != filePieceLimit2.StartPiece {
		return false
	}

	if filePieceLimit1.StartPieceOffset != filePieceLimit2.StartPieceOffset {
		return false
	}

	if filePieceLimit1.EndPiece != filePieceLimit2.EndPiece {
		return false
	}

	if filePieceLimit1.EndPieceOffset != filePieceLimit2.EndPieceOffset {
		return false
	}

	if !CompareIntToIntMap(filePieceLimit1.Pieces, filePieceLimit2.Pieces) {
		return false
	}

	return true
}

func CompareIntToIntMap(map1, map2 map[int]int) bool {
	if len(map1) != len(map2) {
		return false
	}

	for key, _ := range map1 {
		if map1[key] != map2[key] {
			return false
		}
	}

	return true
}
