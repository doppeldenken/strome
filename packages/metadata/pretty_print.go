package metadata

import "fmt"

//nolint
func (torrentMetadata TorrentMetadata) PrettyPrint() {
	fmt.Print("\n--- TORRENT INFO ---\n\n")

	fmt.Println(
		fmt.Sprintf(
			"Name: %v",
			torrentMetadata.Info.Name,
		),
	)

	fmt.Println(
		fmt.Sprintf(
			"Announce: %v",
			torrentMetadata.Announce,
		),
	)

	fmt.Println(
		fmt.Sprintf(
			"Comment: %v",
			torrentMetadata.Comment,
		),
	)

	fmt.Println(
		fmt.Sprintf(
			"Creation Date: %v",
			torrentMetadata.CreationDate,
		),
	)

	fmt.Println(
		fmt.Sprintf(
			"Length: %v",
			torrentMetadata.Info.Length,
		),
	)

	fmt.Println(
		fmt.Sprintf(
			"Piece Length: %v",
			torrentMetadata.Info.PieceLength,
		),
	)

	if len(torrentMetadata.Info.Files) > 0 {
		fmt.Println("Files:")
		for _, file := range torrentMetadata.Info.Files {
			fmt.Println(
				fmt.Sprintf(
					"	Path: %v",
					file.Path,
				),
			)

			fmt.Println(
				fmt.Sprintf(
					"	Length: %v",
					file.Length,
				),
			)
		}
	}

	fmt.Print("\n\n--- TORRENT INFO ---\n\n")
}
