package metadata

type ITorrentMetadata interface {
	PrettyPrint()
}

type TorrentMetadata struct {
	Announce     string              `bencode:"announce"`
	Comment      string              `bencode:"comment"`
	CreationDate int                 `bencode:"creation date"`
	Info         TorrentMetadataInfo `bencode:"info"`
}
