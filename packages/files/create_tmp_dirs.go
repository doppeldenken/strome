package files

//nolint
import (
	"doppeldenken/strome/packages/metadata"
	"doppeldenken/strome/packages/utils"

	"fmt"
	"os"
	"strings"
)

func CreateTmpDirs(
	torrentMetadataInfo metadata.TorrentMetadataInfo,
	dirAdapter utils.IDirAdapter,
) (
	torrentTmpDirRoot string,
	err error,
) {
	infoHash, err := torrentMetadataInfo.GetTorrentSha1Hash()
	if err != nil {
		return "", err
	}

	infoHashString := fmt.Sprintf("%x", infoHash)

	if len(torrentMetadataInfo.Files) == 0 &&
		torrentMetadataInfo.Name != "" &&
		torrentMetadataInfo.Length != 0 {
		torrentMetadataInfo.Files = []metadata.TorrentMetadataInfoFile{
			{
				Length: torrentMetadataInfo.Length,
				Path:   []string{torrentMetadataInfo.Name},
			},
		}
	}

	if len(torrentMetadataInfo.Files) == 0 && (torrentMetadataInfo.Name == "" || torrentMetadataInfo.Length != 0) {
		return "", NoFileSpecifiedError{}
	}

	torrentTmpDirRoot, err = createTmpDirs(
		torrentMetadataInfo.Files,
		infoHashString,
		dirAdapter,
	)
	if err != nil {
		return "", err
	}

	return torrentTmpDirRoot, nil
}

func createTmpDirs(
	files []metadata.TorrentMetadataInfoFile,
	infoHash string,
	dirAdapter utils.IDirAdapter,
) (
	torrentRootTmpDir string,
	err error,
) {
	torrentRootTmpDir = fmt.Sprintf("%v/%v", StromeTmpDir, infoHash)

	err = dirAdapter.MkdirAll(
		fmt.Sprintf(
			"%v/%v",
			torrentRootTmpDir,
			".pieces",
		),
		os.ModeDir,
	)
	if err != nil {
		return "", err
	}

	for _, file := range files {
		paths := len(file.Path)

		if paths > 1 {
			err = dirAdapter.MkdirAll(
				fmt.Sprintf(
					"%v/%v",
					torrentRootTmpDir,
					strings.Join(file.Path[:paths-1], "/"),
				),
				os.ModeDir,
			)

			if err != nil {
				return "", err
			}
		}
	}

	return torrentRootTmpDir, nil
}
