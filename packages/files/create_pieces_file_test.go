package files_test

//nolint
import (
	"bytes"
	"doppeldenken/strome/packages/files"
	"doppeldenken/strome/packages/mocks"

	"errors"
	"io/fs"
	"os"
	"testing"
)

func TestCreatePiecesFile(t *testing.T) {
	t.Parallel()

	testCases := getCreatePiecesFileTestCases()

	for _, testCase := range testCases {
		testCase := testCase

		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()

			writtenJsons := [][]byte{}
			testCase.mockFileAdapter.writtenJsons = &writtenJsons

			err := files.CreatePiecesFile(
				testCase.numberOfPieces,
				testCase.torrentTmpDir,
				testCase.mockFileAdapter,
				testCase.mockJsonAdapter,
				testCase.mockOsAdapter,
			)

			if !errors.Is(err, testCase.expectedErr) {
				t.Errorf(
					"got error '%v', expected error '%v'",
					err,
					testCase.expectedErr,
				)
			}

			if !compareWrittenJsons(writtenJsons, testCase.expectedWrittenJsons) {
				t.Errorf(
					"got writtenJsons '%v', expected writtenJsons '%v'",
					writtenJsons,
					testCase.expectedWrittenJsons,
				)
			}
		})
	}
}

type mockFileAdapter struct {
	writeError   bool
	writtenJsons *[][]byte
}

func (fileAdapter mockFileAdapter) Name(file *os.File, fullPath bool) (name string, err error) {
	return "", nil
}

func (fileAdapter mockFileAdapter) Read(name string) (buffer bytes.Buffer, err error) {
	return buffer, nil
}

func (fileAdapter mockFileAdapter) Write(file *os.File, data []byte, offset int) (n int, err error) {
	if fileAdapter.writeError {
		return 0, os.ErrClosed
	}

	*fileAdapter.writtenJsons = append(*fileAdapter.writtenJsons, data)

	return 1, nil
}

type mockJsonAdapter struct {
	marshalIndentError bool
}

func (jsonAdapter mockJsonAdapter) Marshal(v interface{}) (data []byte, err error) {
	return []byte{}, nil
}

func (jsonAdapter mockJsonAdapter) MarshalIndent(v interface{}, prefix string, indent string) ([]byte, error) {
	if jsonAdapter.marshalIndentError {
		return []byte{}, os.ErrClosed
	}

	return []byte{
		123, 10, 32, 32, 34, 49, 34, 58, 32, 102, 97, 108, 115, 101, 44, 10, 32, 32, 34, 49, 48, 34, 58, 32, 102, 97, 108, 115, 101, 44, 10, 32, 32, 34, 50, 34, 58, 32, 102, 97, 108, 115, 101, 44, 10, 32, 32, 34, 51, 34, 58, 32, 102, 97, 108, 115, 101, 44, 10, 32, 32, 34, 52, 34, 58, 32, 102, 97, 108, 115, 101, 44, 10, 32, 32, 34, 53, 34, 58, 32, 102, 97, 108, 115, 101, 44, 10, 32, 32, 34, 54, 34, 58, 32, 102, 97, 108, 115, 101, 44, 10, 32, 32, 34, 55, 34, 58, 32, 102, 97, 108, 115, 101, 44, 10, 32, 32, 34, 56, 34, 58, 32, 102, 97, 108, 115, 101, 44, 10, 32, 32, 34, 57, 34, 58, 32, 102, 97, 108, 115, 101, 10, 125, //nolint
	}, nil
}

func (jsonAdapter mockJsonAdapter) Unmarshal(data []byte, v interface{}) error {
	return nil
}

type mockOsAdapter struct {
	piecesFileAlreadyExists bool
	fileCreationError       bool
}

func (osAdapter mockOsAdapter) Create(name string, truncate bool) (*os.File, error) {
	if osAdapter.fileCreationError {
		return nil, os.ErrClosed
	}

	return nil, nil
}

func (osAdapter mockOsAdapter) Executable() (string, error) {
	return os.Executable()
}

func (osAdapter mockOsAdapter) Read(name string) ([]byte, error) {
	return []byte{}, nil
}

func (osAdapter mockOsAdapter) ReadDir(name string) (entries []fs.DirEntry, err error) {
	return []fs.DirEntry{}, nil
}

func (osAdapter mockOsAdapter) Remove(name string) error {
	return nil
}

func (osAdapter mockOsAdapter) Stat(name string) (fs.FileInfo, error) {
	if osAdapter.piecesFileAlreadyExists == true {
		return mocks.MockFileInfo{}, nil
	}

	return nil, nil
}

type createPiecesFileTestCase struct {
	name                 string
	numberOfPieces       int
	torrentTmpDir        string
	mockFileAdapter      mockFileAdapter
	mockJsonAdapter      mockJsonAdapter
	mockOsAdapter        mockOsAdapter
	expectedErr          error
	expectedWrittenJsons [][]byte
}

func compareWrittenJsons(writtenJsons1, writtenJsons2 [][]byte) bool {
	if len(writtenJsons1) != len(writtenJsons2) {
		return false
	}

	for i := 0; i < len(writtenJsons1); i++ {
		if !bytes.Equal(writtenJsons1[i], writtenJsons2[i]) {
			return false
		}
	}

	return true
}

//nolint:funlen
func getCreatePiecesFileTestCases() []createPiecesFileTestCase {
	return []createPiecesFileTestCase{
		{
			name:           "successful pieces file creation",
			numberOfPieces: 10,
			torrentTmpDir:  "/tmp",
			mockFileAdapter: mockFileAdapter{
				writeError: false,
			},
			mockJsonAdapter: mockJsonAdapter{
				marshalIndentError: false,
			},
			mockOsAdapter: mockOsAdapter{
				piecesFileAlreadyExists: false,
				fileCreationError:       false,
			},
			expectedErr: nil,
			expectedWrittenJsons: [][]byte{
				{123, 10, 32, 32, 34, 49, 34, 58, 32, 102, 97, 108, 115, 101, 44, 10, 32, 32, 34, 49, 48, 34, 58, 32, 102, 97, 108, 115, 101, 44, 10, 32, 32, 34, 50, 34, 58, 32, 102, 97, 108, 115, 101, 44, 10, 32, 32, 34, 51, 34, 58, 32, 102, 97, 108, 115, 101, 44, 10, 32, 32, 34, 52, 34, 58, 32, 102, 97, 108, 115, 101, 44, 10, 32, 32, 34, 53, 34, 58, 32, 102, 97, 108, 115, 101, 44, 10, 32, 32, 34, 54, 34, 58, 32, 102, 97, 108, 115, 101, 44, 10, 32, 32, 34, 55, 34, 58, 32, 102, 97, 108, 115, 101, 44, 10, 32, 32, 34, 56, 34, 58, 32, 102, 97, 108, 115, 101, 44, 10, 32, 32, 34, 57, 34, 58, 32, 102, 97, 108, 115, 101, 10, 125}, //nolint
			},
		},
		{
			name:           "unsuccessful pieces file creation - pieces file already exists",
			numberOfPieces: 0,
			torrentTmpDir:  "",
			mockFileAdapter: mockFileAdapter{
				writeError: false,
			},
			mockJsonAdapter: mockJsonAdapter{
				marshalIndentError: false,
			},
			mockOsAdapter: mockOsAdapter{
				piecesFileAlreadyExists: true,
				fileCreationError:       false,
			},
			expectedErr:          files.PiecesFileAlreadyExistsError{},
			expectedWrittenJsons: [][]byte{},
		},
		{
			name:           "unsuccessful pieces file creation - marshal indent error",
			numberOfPieces: 0,
			torrentTmpDir:  "",
			mockFileAdapter: mockFileAdapter{
				writeError: false,
			},
			mockJsonAdapter: mockJsonAdapter{
				marshalIndentError: true,
			},
			mockOsAdapter: mockOsAdapter{
				piecesFileAlreadyExists: false,
				fileCreationError:       false,
			},
			expectedErr:          os.ErrClosed,
			expectedWrittenJsons: [][]byte{},
		},
		{
			name:           "unsuccessful pieces file creation - file creation error",
			numberOfPieces: 0,
			torrentTmpDir:  "",
			mockFileAdapter: mockFileAdapter{
				writeError: false,
			},
			mockJsonAdapter: mockJsonAdapter{
				marshalIndentError: false,
			},
			mockOsAdapter: mockOsAdapter{
				piecesFileAlreadyExists: false,
				fileCreationError:       true,
			},
			expectedErr:          os.ErrClosed,
			expectedWrittenJsons: [][]byte{},
		},
		{
			name:           "unsuccessful pieces file creation - write error",
			numberOfPieces: 0,
			torrentTmpDir:  "",
			mockFileAdapter: mockFileAdapter{
				writeError: true,
			},
			mockJsonAdapter: mockJsonAdapter{
				marshalIndentError: false,
			},
			mockOsAdapter: mockOsAdapter{
				piecesFileAlreadyExists: false,
				fileCreationError:       false,
			},
			expectedErr:          os.ErrClosed,
			expectedWrittenJsons: [][]byte{},
		},
	}
}
