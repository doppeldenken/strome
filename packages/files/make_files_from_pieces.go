package files

//nolint
import (
	"doppeldenken/strome/packages/download_context"
	"doppeldenken/strome/packages/metadata"
	"doppeldenken/strome/packages/observability"
	"doppeldenken/strome/packages/utils"

	"fmt"
	"io/fs"
	"os"
	"strconv"
	"strings"
)

func MakeFilesFromPieces(
	allFilesCompletedChannel chan bool,
	dirAdapter utils.IDirAdapter,
	downloadContext download_context.DownloadContext,
	fileAdapter utils.IFileAdapter,
	filePieceLimits metadata.FilePieceLimits,
	jsonAdapter utils.IJsonAdapter,
	osAdapter utils.IOsAdapter,
	stromeRunningPath string,
	torrentMetadata metadata.TorrentMetadata,
	torrentRootTmpDir string,
) error {
	logger := observability.GetLogger(-1)

	piecesFolderFullName := fmt.Sprintf(
		"%v/.pieces",
		torrentRootTmpDir,
	)

	pieces, err := osAdapter.ReadDir(piecesFolderFullName)
	if err != nil {
		return err
	}

	logger.Log(
		fmt.Sprintf(
			"number of pieces in tmp folder: %v",
			len(pieces),
		),
		observability.DebugLogLevel,
	)

	consumedPieces := []int{}

	if len(torrentMetadata.Info.Files) == 0 {
		consumedPieces, err = createOneFileTorrent(
			downloadContext,
			fileAdapter,
			osAdapter,
			pieces,
			piecesFolderFullName,
			stromeRunningPath,
			torrentMetadata,
		)
	} else {
		consumedPieces, err = createMultipleFilesTorrent(
			downloadContext,
			dirAdapter,
			fileAdapter,
			filePieceLimits,
			osAdapter,
			pieces,
			piecesFolderFullName,
			stromeRunningPath,
			torrentMetadata,
		)
	}

	if err != nil {
		logger.Log(
			fmt.Sprintf(
				"an error occurred when adding pieces to files: %v",
				err,
			),
			observability.DebugLogLevel,
		)

		// TODO: panic?
	}

	logger.Log(
		fmt.Sprintf(
			"consumed pieces: %v",
			consumedPieces,
		),
		observability.DebugLogLevel,
	)

	downloadedPieces, err := updatePiecesFile(consumedPieces, torrentRootTmpDir)
	if err != nil {
		logger.Log(
			fmt.Sprintf(
				"error updating pieces file: %v",
				err,
			),
			observability.DebugLogLevel,
		)

		// TODO: panic?
	}

	if checkIfAllPiecesAreDownloaded(downloadedPieces) {
		allFilesCompletedChannel <- true
	}

	logger.Log(
		"downloaded pieces file updated successfully",
		observability.DebugLogLevel,
	)

	return nil
}

func checkIfAllPiecesAreDownloaded(downloadedPieces DownloadedPieces) bool {
	for key, _ := range downloadedPieces {
		if downloadedPieces[key] == false {
			return false
		}
	}

	return true
}

func getTorrentDir(stromeRunningPath string, torrentMetadata metadata.TorrentMetadata) string {
	if len(torrentMetadata.Info.Files) == 0 {
		return stromeRunningPath
	} else {
		return fmt.Sprintf(
			"%v/%v",
			stromeRunningPath,
			torrentMetadata.Info.Name,
		)
	}
}

func createMultipleFilesTorrent(
	downloadContext download_context.DownloadContext,
	dirAdapter utils.IDirAdapter,
	fileAdapter utils.IFileAdapter,
	filePieceLimits metadata.FilePieceLimits,
	osAdapter utils.IOsAdapter,
	pieces []fs.DirEntry,
	piecesFolderFullName string,
	stromeRunningPath string,
	torrentMetadata metadata.TorrentMetadata,
) (
	consumedPieces []int,
	err error,
) {
	logger := observability.GetLogger(-1)

	_, err = makeTorrentDir(
		dirAdapter,
		torrentMetadata.Info.Name,
		stromeRunningPath,
	)
	if err != nil {
		return []int{}, err
	}

	piecesAsInts, err := getPiecesAsInts(pieces)
	if err != nil {
		return []int{}, err
	}

	for _, file := range torrentMetadata.Info.Files {
		filePointer, err := getTorrentFile(
			dirAdapter,
			file,
			osAdapter,
			stromeRunningPath,
		)

		if err != nil {
			return consumedPieces, err
		}

		filename := file.Path[len(file.Path)-1]

		filePieceLimit := filePieceLimits[filename]

		for i, piece := range piecesAsInts {
			if _, exists := filePieceLimit.Pieces[piece]; exists {
				pieceNameStr := pieces[i].Name()

				pieceFullName := fmt.Sprintf(
					"%v/%v",
					piecesFolderFullName,
					pieceNameStr,
				)

				pieceBytes, err := osAdapter.Read(pieceFullName)
				if err != nil {
					return consumedPieces, err
				}

				start := 0
				end := 0
				fileOffset := filePieceLimit.Pieces[piece]

				if piece == filePieceLimit.StartPiece && piece == filePieceLimit.EndPiece {
					start = filePieceLimit.StartPieceOffset
					end = filePieceLimit.EndPieceOffset
				} else if piece == filePieceLimit.StartPiece {
					start = filePieceLimit.StartPieceOffset
					end = len(pieceBytes)
				} else if piece == filePieceLimit.EndPiece {
					start = 0
					end = filePieceLimit.EndPieceOffset
				} else {
					start = 0
					end = len(pieceBytes)
				}

				_, err = fileAdapter.Write(
					filePointer,
					pieceBytes[start:end],
					fileOffset,
				)
				if err != nil {
					return consumedPieces, err
				}

				logger.Log(
					fmt.Sprintf(
						"written piece '%v' to file '%v'",
						pieceNameStr,
						filename,
					),
					observability.DebugLogLevel,
				)

				if start == 0 && end == len(pieceBytes) {
					err = osAdapter.Remove(pieceFullName)
					if err != nil {
						return consumedPieces, err
					}

					logger.Log(
						fmt.Sprintf(
							"deleted piece '%v'",
							pieceNameStr,
						),
						observability.DebugLogLevel,
					)
				}

				consumedPieces = append(consumedPieces, piece)
			}
		}
	}

	return consumedPieces, nil
}

func getPiecesAsInts(pieces []fs.DirEntry) (piecesAsInts []int, err error) {
	for _, piece := range pieces {
		pieceInt, err := strconv.Atoi(piece.Name())
		if err != nil {
			return []int{}, err
		}

		piecesAsInts = append(piecesAsInts, pieceInt)
	}

	return piecesAsInts, nil
}

func makeTorrentDir(
	dirAdapter utils.IDirAdapter,
	dirName,
	stromeRunningPath string,
) (
	torrentDirPath string,
	err error,
) {
	torrentDirPath = fmt.Sprintf(
		"%v/%v",
		stromeRunningPath,
		dirName,
	)

	err = dirAdapter.MkdirAll(
		torrentDirPath,
		fs.ModeDir,
	)

	if err != nil {
		return "", err
	}

	observability.GetLogger(-1).Log(
		fmt.Sprintf(
			"torrent folder: %v",
			torrentDirPath,
		),
		observability.DebugLogLevel,
	)

	return torrentDirPath, nil
}

func getTorrentFile(
	dirAdapter utils.IDirAdapter,
	file metadata.TorrentMetadataInfoFile,
	osAdapter utils.IOsAdapter,
	stromeRunningDir string,
) (
	*os.File,
	error,
) {
	fileFullPath := fmt.Sprintf(
		"%v/%v",
		stromeRunningDir,
		strings.Join(file.Path, "/"),
	)

	fileAbsoluteDir := fmt.Sprintf(
		"%v/%v",
		stromeRunningDir,
		file.Path[len(file.Path)-1],
	)

	err := dirAdapter.MkdirAll(fileAbsoluteDir, os.ModeDir)
	if err != nil {
		return nil, err
	}

	observability.GetLogger(-1).Log(
		fmt.Sprintf(
			"creating file: %v",
			fileAbsoluteDir,
		),
		observability.DebugLogLevel,
	)

	return osAdapter.Create(fileFullPath, false)
}

func createOneFileTorrent(
	downloadContext download_context.DownloadContext,
	fileAdapter utils.IFileAdapter,
	osAdapter utils.IOsAdapter,
	pieces []fs.DirEntry,
	piecesFolderFullName string,
	stromeRunningPath string,
	torrentMetadata metadata.TorrentMetadata,
) (
	consumedPieces []int,
	err error,
) {
	logger := observability.GetLogger(-1)

	filename := fmt.Sprintf(
		"%v/%v",
		stromeRunningPath,
		torrentMetadata.Info.Name,
	)

	file, err := osAdapter.Create(
		filename,
		false,
	)
	if err != nil {
		return []int{}, err
	}

	logger.Log(
		fmt.Sprintf(
			"file '%v' created successfully",
			filename,
		),
		observability.DebugLogLevel,
	)

	defer file.Close()

	for _, piece := range pieces {
		pieceNameStr := piece.Name()

		pieceNameInt, err := strconv.Atoi(pieceNameStr)
		if err != nil {
			return consumedPieces, err
		}

		fileinfo, err := piece.Info()
		if err != nil {
			return consumedPieces, err
		}

		if pieceNameInt != downloadContext.LastPieceNumber && fileinfo.Size() != int64(torrentMetadata.Info.PieceLength) {
			logger.Log(
				fmt.Sprintf(
					"piece '%v' not complete; skipping",
					pieceNameStr,
				),
				observability.DebugLogLevel,
			)

			continue
		} else if pieceNameInt == downloadContext.LastPieceNumber && fileinfo.Size() != int64(downloadContext.LastPieceSize) {
			logger.Log(
				fmt.Sprintf(
					"piece '%v' not complete; skipping",
					pieceNameStr,
				),
				observability.DebugLogLevel,
			)

			continue
		}

		pieceFullName := fmt.Sprintf(
			"%v/%v",
			piecesFolderFullName,
			pieceNameStr,
		)

		pieceBytes, err := osAdapter.Read(pieceFullName)
		if err != nil {
			return consumedPieces, err
		}

		offset := pieceNameInt * torrentMetadata.Info.PieceLength

		_, err = fileAdapter.Write(file, pieceBytes, offset)
		if err != nil {
			return consumedPieces, err
		}

		err = osAdapter.Remove(pieceFullName)
		if err != nil {
			return consumedPieces, err
		}

		logger.Log(
			fmt.Sprintf(
				"piece '%v' written successfully to file '%v'",
				pieceNameStr,
				filename,
			),
			observability.DebugLogLevel,
		)

		consumedPieces = append(consumedPieces, pieceNameInt)
	}

	return consumedPieces, nil
}
