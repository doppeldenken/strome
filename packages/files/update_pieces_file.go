package files

//nolint
import (
	"doppeldenken/strome/packages/utils"

	"fmt"
)

func updatePiecesFile(
	pieceNumbers []int,
	torrentTmpRootDir string,
) (
	downloadedPieces DownloadedPieces,
	err error,
) {
	downloadedPieces, err = getDownloadedPieces(torrentTmpRootDir)
	if err != nil {
		return DownloadedPieces{}, err
	}

	for _, piece := range pieceNumbers {
		downloadedPieces[piece] = true
	}

	jsonAdapter := utils.JsonAdapter{}

	downloadedPiecesFileBytes, err := jsonAdapter.Marshal(downloadedPieces)
	if err != nil {
		return DownloadedPieces{}, err
	}

	osAdapter := utils.OsAdapter{}

	downloadedPiecesFile, err := osAdapter.Create(
		fmt.Sprintf(
			"%v/%v",
			torrentTmpRootDir,
			PiecesFileName,
		),
		true,
	)
	if err != nil {
		return DownloadedPieces{}, err
	}

	defer downloadedPiecesFile.Close()

	fileAdapter := utils.FileAdapter{}

	_, err = fileAdapter.Write(downloadedPiecesFile, downloadedPiecesFileBytes, -1)
	if err != nil {
		return DownloadedPieces{}, err
	}

	return downloadedPieces, nil
}
