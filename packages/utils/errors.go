package utils

import "fmt"

type BadFilenameError struct {
	Filename string
}

func (badFileNameError BadFilenameError) Error() string {
	return fmt.Sprintf("bad filename '%v'", badFileNameError.Filename)
}
