package utils

import (
	"fmt"
	"net"
)

func GetPeerTcpConnection(url, port string) (conn net.Conn, err error) {
	address := fmt.Sprintf("%v:%v", url, port)

	return net.DialTimeout("tcp", address, PeerTimeout)
}
