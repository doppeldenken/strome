package utils

import (
	"bytes"
	"io"
	"os"
)

func (fileAdapter FileAdapter) Read(name string) (buffer bytes.Buffer, err error) {
	file, err := os.Open(name)
	if err != nil {
		return buffer, err
	}

	defer file.Close()

	bytesToRead := 16

	for err != io.EOF {
		byteSlice := make([]byte, bytesToRead)
		bytesRead, readErr := file.Read(byteSlice)

		if readErr != io.EOF && err != nil {
			return buffer, err
		}

		if bytesRead > 0 {
			buffer.Write(byteSlice[:bytesRead])
		}

		err = readErr
	}

	return buffer, nil
}
