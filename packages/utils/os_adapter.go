package utils

import (
	"io/fs"
	"os"
)

type IOsAdapter interface {
	Create(name string, truncate bool) (file *os.File, err error)
	Executable() (string, error)
	Read(name string) ([]byte, error)
	ReadDir(name string) (entries []fs.DirEntry, err error)
	Remove(name string) error
	Stat(name string) (fs.FileInfo, error)
}

type OsAdapter struct{}

func (osAdapter OsAdapter) Create(name string, truncate bool) (file *os.File, err error) {
	fileinfo, _ := osAdapter.Stat(name)

	if fileinfo != nil && !truncate {
		return os.OpenFile(name, os.O_RDWR, os.ModeAppend)
	}

	file, err = os.Create(name)
	if err != nil {
		return nil, err
	}

	return file, nil
}

func (osAdapter OsAdapter) Executable() (string, error) {
	return os.Executable()
}

func (osAdapter OsAdapter) Read(name string) ([]byte, error) {
	return os.ReadFile(name)
}

func (osAdapter OsAdapter) ReadDir(name string) (entries []fs.DirEntry, err error) {
	entries, err = os.ReadDir(name)
	if err != nil {
		return []fs.DirEntry{}, err
	}

	return entries, err
}

func (osAdapter OsAdapter) Remove(name string) error {
	return os.Remove(name)
}

func (osAdapter OsAdapter) Stat(name string) (fs.FileInfo, error) {
	return os.Stat(name)
}
