package utils

import "fmt"

type PeerConnectionError struct {
	Reason string
}

func (peerConnectionError PeerConnectionError) Error() string {
	return fmt.Sprintf("error connecting to peer: '%v'", peerConnectionError.Reason)
}
