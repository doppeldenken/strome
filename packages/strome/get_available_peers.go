package strome

//nolint
import (
	"doppeldenken/strome/packages/metadata"
	"doppeldenken/strome/packages/observability"
	"doppeldenken/strome/packages/tracker"

	"fmt"
	"time"
)

func getAvailablePeers(torrentMetadata metadata.TorrentMetadata) (availablePeers map[string]tracker.Peer) {
	logger := observability.GetLogger(-1)

	logger.Log("getting more peers from tracker", observability.InfoLogLevel)

	availablePeers = make(map[string]tracker.Peer)

	response, err := tracker.Get(torrentMetadata)
	if err != nil {
		logger.Log(
			fmt.Sprintf(
				"error getting tracker info: '%v'",
				err,
			),
			observability.DebugLogLevel,
		)

		logger.Log("sleeping for 30 seconds before trying the tracker again", observability.DebugLogLevel)

		time.Sleep(30 * time.Second)

		getAvailablePeers(torrentMetadata)
	}

	logger.Log(
		fmt.Sprintf(
			"tracker response for torrent '%v' successful; number of peers: %v",
			torrentMetadata.Info.Name,
			len(response.Peers),
		),
		observability.InfoLogLevel,
	)

	for _, peer := range response.Peers {
		availablePeers[peer.Ip] = peer
	}

	return availablePeers
}
