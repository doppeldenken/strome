package strome

import (
	"doppeldenken/strome/packages/download"
	"doppeldenken/strome/packages/download_context"
	"doppeldenken/strome/packages/metadata"
	"doppeldenken/strome/packages/utils"
)

func prepareEnvForDownload(
	downloadContext download_context.DownloadContext,
	torrentMetadata metadata.TorrentMetadata,
) (
	downloadFileAdapter download.DownloadFileAdapter,
	piecesToDownload []int,
) {
	torrentRootTmpDir := createTmpDirs(torrentMetadata)

	createPiecesFile(
		torrentMetadata,
		downloadContext.NumberOfPieces,
		torrentRootTmpDir,
	)

	downloadFileAdapter = download.DownloadFileAdapter{
		FileAdapter:       utils.FileAdapter{},
		JsonAdapter:       utils.JsonAdapter{},
		NumberOfPieces:    downloadContext.NumberOfPieces,
		OsAdapter:         utils.OsAdapter{},
		PieceLength:       torrentMetadata.Info.PieceLength,
		TorrentTmpRootDir: torrentRootTmpDir,
	}

	cleanIncompletePieces(downloadFileAdapter)

	piecesToDownload = getPiecesToDownload(downloadFileAdapter)

	return downloadFileAdapter, piecesToDownload
}
