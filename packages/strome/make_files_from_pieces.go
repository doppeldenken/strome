package strome

//nolint
import (
	"doppeldenken/strome/packages/download_context"
	"doppeldenken/strome/packages/files"
	"doppeldenken/strome/packages/metadata"
	"doppeldenken/strome/packages/observability"
	"doppeldenken/strome/packages/utils"
	"fmt"
	"strings"

	"os"
	"time"
)

func makeFilesFromPieces(
	downloadContext download_context.DownloadContext,
	torrentMetadata metadata.TorrentMetadata,
	torrentRootTmpDir string,
) {
	allFilesCompletedChannel := make(chan bool)

	go func() {
		logger := observability.GetLogger(-1)

		dirAdapter := utils.DirAdapter{}
		fileAdapter := utils.FileAdapter{}
		osAdapter := utils.OsAdapter{}

		filePieceLimits := torrentMetadata.Info.CalculateFilePieceLimits()

		stromeRunningPath, err := osAdapter.Executable()
		if err != nil {
			logger.Log(err.Error(), observability.InfoLogLevel)

			os.Exit(0)
		}

		stromeRunningPathAsArray := strings.Split(stromeRunningPath, "/")

		stromeRunningPath = strings.Join(stromeRunningPathAsArray[:len(stromeRunningPathAsArray)-1], "/")

		logger.Log(
			fmt.Sprintf(
				"strome running path: %v",
				stromeRunningPath,
			),
			observability.DebugLogLevel,
		)

		for {
			err := files.MakeFilesFromPieces(
				allFilesCompletedChannel,
				dirAdapter,
				downloadContext,
				fileAdapter,
				filePieceLimits,
				utils.JsonAdapter{},
				osAdapter,
				stromeRunningPath,
				torrentMetadata,
				torrentRootTmpDir,
			)
			checkError(err)

			time.Sleep(waitingPeriod * time.Second)
		}
	}()

	select {
	case <-allFilesCompletedChannel:
		observability.GetLogger(-1).Log("all files are downloaded!", observability.InfoLogLevel)

		removeTmpFiles(torrentRootTmpDir)

		os.Exit(0)
	}
}
