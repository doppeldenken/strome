package strome

//nolint
import (
	"doppeldenken/strome/packages/metadata"
	"doppeldenken/strome/packages/tracker"

	"time"
)

func ongoingPeerRefresh(
	availablePeersQueue chan tracker.Peer,
	getMorePeersChannel chan bool,
	torrentMetadata metadata.TorrentMetadata,
) {
	getMorePeersChannel <- true

	lockInMilliseconds := time.Now().UnixMilli()

	for {
		select {
		case <-getMorePeersChannel:
			if time.Now().UnixMilli()-lockInMilliseconds < 30000 {
				break
			}

			lockInMilliseconds = time.Now().UnixMilli()

			availablePeers := getAvailablePeers(torrentMetadata)

			for _, availablePeer := range availablePeers {
				go func(peer tracker.Peer) {
					availablePeersQueue <- peer
				}(availablePeer)
			}

			time.Sleep(1 * time.Second)
		}
	}
}
