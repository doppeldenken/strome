package strome

import (
	"doppeldenken/strome/packages/metadata"
	"doppeldenken/strome/packages/tracker"
)

func initializeChannels(
	torrentMetadata metadata.TorrentMetadata,
	numberOfPieces int,
	piecesToDownload []int,
) (
	availablePeersQueue chan tracker.Peer,
	completedPieceCountChannel,
	getMorePeersChannel,
	maxConnectionsQueue chan bool,
	piecesToDownloadQueue chan int,
) {
	availablePeersQueue = make(chan tracker.Peer, maxNumberOfQueuedPeers)
	availablePeers := getAvailablePeers(torrentMetadata)

	for _, availablePeer := range availablePeers {
		go func(peer tracker.Peer) {
			availablePeersQueue <- peer
		}(availablePeer)
	}

	completedPieceCountChannel = make(chan bool, numberOfPieces+1)

	getMorePeersChannel = make(chan bool, maxNumberOfQueuedPeers)

	maxConnectionsQueue = make(chan bool, maxConnections)

	for i := 0; i < maxConnections; i++ {
		go func(conn bool) {
			maxConnectionsQueue <- conn
		}(true)
	}

	piecesToDownloadQueue = make(chan int, numberOfPieces+1)

	for _, pieceToDownload := range piecesToDownload {
		go func(piece int) {
			piecesToDownloadQueue <- piece
		}(pieceToDownload)
	}

	return availablePeersQueue,
		completedPieceCountChannel,
		getMorePeersChannel,
		maxConnectionsQueue,
		piecesToDownloadQueue
}
