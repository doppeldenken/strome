package strome

//nolint
import (
	"doppeldenken/strome/packages/files"
	"doppeldenken/strome/packages/metadata"
	"doppeldenken/strome/packages/observability"
	"doppeldenken/strome/packages/utils"

	"errors"
	"fmt"
)

func createPiecesFile(
	metadata metadata.TorrentMetadata,
	numberOfPieces int,
	torrentRootTmpDir string) {
	fileAdapter := utils.FileAdapter{}
	jsonAdapter := utils.JsonAdapter{}
	osAdapter := utils.OsAdapter{}

	err := files.CreatePiecesFile(
		numberOfPieces,
		torrentRootTmpDir,
		fileAdapter,
		jsonAdapter,
		osAdapter,
	)

	if !errors.Is(err, files.PiecesFileAlreadyExistsError{}) {
		checkError(err)
	}

	logger := observability.GetLogger(-1)

	logger.Log(
		fmt.Sprintf(
			"pieces file created successfully for torrent: %v",
			metadata.Info.Name,
		),
		observability.DebugLogLevel,
	)
}
