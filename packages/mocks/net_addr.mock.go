package mocks

type mockNetAddr struct{}

func (mockNetAddr mockNetAddr) Network() string {
	return "tcp"
}

func (mockNetAddr mockNetAddr) String() string {
	return "0.0.0.0"
}
