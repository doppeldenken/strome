package mocks

//nolint
import (
	"doppeldenken/strome/packages/client"

	"net"
	"time"
)

type MockConn struct {
	NumberOfReads      *int
	FirstReadError     bool
	SecondReadError    bool
	OnlyZeroBytesError bool
	WriteError         bool
	Testing            string
}

func (conn MockConn) Close() error {
	return nil
}

func (conn MockConn) LocalAddr() net.Addr {
	return nil
}

func (conn MockConn) Read(b []byte) (n int, err error) {
	*conn.NumberOfReads += 1

	if conn.Testing == "ReceiveResponse" {
		if *conn.NumberOfReads == 1 {
			if conn.FirstReadError {
				return 0, client.EmptyResponseError{
					Client: "0.0.0.0",
				}
			}

			copy(b, []byte{0, 0, 0, 8})
		} else if *conn.NumberOfReads == 2 {
			if conn.SecondReadError {
				return 0, net.ErrClosed
			}

			if conn.OnlyZeroBytesError {
				copy(b, []byte{0, 0, 0, 0, 0, 0, 0, 0})

				return 8, nil
			}

			copy(b, []byte{1, 2, 3, 4, 1, 2, 3, 4})

			return 8, nil
		}
	}

	if conn.Testing == "DownloadPiece" {
		if *conn.NumberOfReads == 1 {
			if conn.FirstReadError {
				return 0, net.ErrClosed
			}

			copy(b, []byte{0, 0, 0, 8})

			return 4, nil
		} else if *conn.NumberOfReads == 2 {
			if conn.SecondReadError {
				return 0, net.ErrClosed
			}

			copy(b, []byte{1, 2, 3, 4, 1, 2, 3, 4})

			return 8, nil
		}
	}

	return n, nil
}

func (conn MockConn) RemoteAddr() net.Addr {
	return mockNetAddr{}
}

func (conn MockConn) SetDeadline(t time.Time) error {
	return nil
}

func (conn MockConn) SetReadDeadline(t time.Time) error {
	return nil
}

func (conn MockConn) SetWriteDeadline(t time.Time) error {
	return nil
}

func (conn MockConn) Write(b []byte) (n int, err error) {
	if conn.Testing == "DownloadPiece" && conn.WriteError {
		return 0, net.ErrWriteToConnected
	}

	return 1, nil
}
