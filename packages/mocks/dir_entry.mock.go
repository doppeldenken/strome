package mocks

import (
	"io/fs"
)

type MockDirEntry struct {
	EntryName            string
	EntryIsDir           bool
	EntryFileInfo        fs.FileInfo
	EntryFileMode        fs.FileMode
	ErrorGettingFileInfo bool
}

func (dirEntry MockDirEntry) Name() string {
	return dirEntry.EntryName
}

func (dirEntry MockDirEntry) IsDir() bool {
	return dirEntry.EntryIsDir
}

func (dirEntry MockDirEntry) Type() fs.FileMode {
	return dirEntry.EntryFileMode
}

func (dirEntry MockDirEntry) Info() (fs.FileInfo, error) {
	if dirEntry.ErrorGettingFileInfo {
		return nil, fs.ErrNotExist
	}

	return dirEntry.EntryFileInfo, nil
}
