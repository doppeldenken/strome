package client_test

//nolint
import (
	"doppeldenken/strome/packages/client"
	"doppeldenken/strome/packages/utils"

	"errors"
	"testing"
)

func TestSerialize(t *testing.T) {
	t.Parallel()

	testCases := getSerializeTestCases()

	for _, testCase := range testCases {
		testCase := testCase

		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()

			buffer, err := testCase.handshake.Serialize()

			if !errors.Is(err, testCase.expectedErr) {
				t.Errorf(
					"got error '%v', expected error '%v'",
					err,
					testCase.expectedErr,
				)
			}

			if !utils.CompareByteSlice(buffer, testCase.expectedBuffer) {
				t.Errorf(
					"got buffer '%v', expected buffer '%v'",
					buffer,
					testCase.expectedBuffer,
				)
			}
		})
	}
}

type serializeHandshakeTest struct {
	name           string
	handshake      client.Handshake
	expectedErr    error
	expectedBuffer []byte
}

func getSerializeTestCases() []serializeHandshakeTest {
	return []serializeHandshakeTest{
		{
			name: "successful serialize",
			handshake: client.Handshake{
				Pstr:          "BitTorrent protocol",
				ReservedBytes: [8]byte{0, 0, 0, 0, 0, 0, 0, 0},
				InfoHash:      [20]byte{66, 117, 1, 213, 227, 64, 149, 227, 66, 96, 139, 120, 68, 201, 24, 91, 217, 197, 247, 19},
				PeerId:        [20]byte{115, 116, 114, 111, 109, 101, 49, 50, 51, 52, 53, 54, 55, 56, 57, 48, 49, 50, 51, 52},
			},
			expectedErr: nil,
			expectedBuffer: []byte{
				19, 66, 105, 116, 84, 111, 114, 114, 101, 110, 116, 32, 112, 114, 111, 116, 111, 99, 111, 108, 0, 0, 0, 0, 0, 0, 0, 0, 66, 117, 1, 213, 227, 64, 149, 227, 66, 96, 139, 120, 68, 201, 24, 91, 217, 197, 247, 19, 115, 116, 114, 111, 109, 101, 49, 50, 51, 52, 53, 54, 55, 56, 57, 48, 49, 50, 51, 52, //nolint
			},
		},
		{
			name: "unsuccessful serialize - buffer size is not of the expected size",
			handshake: client.Handshake{
				Pstr:          "im a bad protocol",
				ReservedBytes: [8]byte{0, 0, 0, 0, 0, 0, 0, 0},
				InfoHash:      [20]byte{66, 117, 1, 213, 227, 64, 149, 227, 66, 96, 139, 120, 68, 201, 24, 91, 217, 197, 247, 19},
				PeerId:        [20]byte{115, 116, 114, 111, 109, 101, 49, 50, 51, 52, 53, 54, 55, 56, 57, 48, 49, 50, 51, 52},
			},
			expectedErr: client.HandshakeSizeError{
				Size: 66,
			},
			expectedBuffer: []byte{},
		},
	}
}
