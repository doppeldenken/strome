package client

const (
	ChokeCode         = 0
	UnchokeCode       = 1
	InterestedCode    = 2
	NotInterestedCode = 3
	HaveCode          = 4
	BitfieldCode      = 5
	RequestCode       = 6
	PieceCode         = 7
	CancelCode        = 8
)

const BlockSize = 16384

const HandshakeReservedBytesSize = 8
