package client

import (
	"fmt"
	"strings"
)

type Bitfield struct {
	Pieces []bool
}

func (bitfield *Bitfield) ParseBitfieldResponse(response []byte) error {
	var tmpBitfield []bool

	for i := 1; i < len(response); i++ {
		bits := fmt.Sprintf("%08b", response[i])

		bitsAsStrings := strings.Split(bits, "")

		var bitsAsBools []bool

		for _, bit := range bitsAsStrings {
			if bit == "1" {
				bitsAsBools = append(bitsAsBools, true)
			} else if bit == "0" {
				bitsAsBools = append(bitsAsBools, false)
			} else {
				return ParsingBitfieldError{
					Reason: fmt.Sprintf(
						"unexpected value in byte number '%v': %v",
						i,
						bits,
					),
				}
			}
		}

		tmpBitfield = append(tmpBitfield, bitsAsBools...)
	}

	bitfield.Pieces = tmpBitfield

	return nil
}
