package client_test

//nolint
import (
	"bytes"

	"doppeldenken/strome/packages/client"

	"testing"
)

func TestMessageSerialize(t *testing.T) {
	t.Parallel()

	testCases := getTestMessageSerializeTestCases()

	for _, testCase := range testCases {
		testCase := testCase

		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()

			serializedMessage := testCase.message.Serialize()

			if !bytes.Equal(serializedMessage, testCase.expectedSerializedMessage) {
				t.Errorf(
					"got serialized message '%v', expected serialized message '%v'",
					serializedMessage,
					testCase.expectedSerializedMessage,
				)
			}
		})
	}
}

type messageSerializeTestCase struct {
	name                      string
	message                   client.Message
	expectedSerializedMessage []byte
}

func getTestMessageSerializeTestCases() []messageSerializeTestCase {
	return []messageSerializeTestCase{
		{
			name: "successful serialize",
			message: client.Message{
				Code: client.BitfieldCode,
				Payload: []byte{
					1, 2, 3, 4, 5, 6, 7, 8, 9,
				},
			},
			expectedSerializedMessage: []byte{
				0, 0, 0, 10, 1, 1, 2, 3, 4, 5, 6, 7, 8, 9,
			},
		},
	}
}
