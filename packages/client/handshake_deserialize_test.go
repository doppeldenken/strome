package client_test

//nolint
import (
	"doppeldenken/strome/packages/client"

	"errors"
	"testing"
)

func TestDeserialize(t *testing.T) {
	t.Parallel()

	testCases := getDeserializeTestCases()

	for _, testCase := range testCases {
		testCase := testCase

		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()

			receivedHandshake := client.Handshake{}
			err := receivedHandshake.Deserialize(testCase.buffer)

			if !errors.Is(err, testCase.expectedErr) {
				t.Errorf(
					"got error '%v', expected error '%v'",
					err,
					testCase.expectedErr,
				)
			}

			if !client.CompareHandshakes(receivedHandshake, testCase.expectedHandshake) {
				t.Errorf(
					"got handshake '%v', expected handshake '%v'",
					receivedHandshake,
					testCase.expectedHandshake,
				)
			}
		})
	}
}

type deserializeHandshakeTest struct {
	name              string
	buffer            []byte
	expectedErr       error
	expectedHandshake client.Handshake
}

func getDeserializeTestCases() []deserializeHandshakeTest {
	return []deserializeHandshakeTest{
		{
			name: "successful deserialize",
			buffer: []byte{
				19, 66, 105, 116, 84, 111, 114, 114, 101, 110, 116, 32, 112, 114, 111, 116, 111, 99, 111, 108, 0, 0, 0, 0, 0, 0, 0, 0, 66, 117, 1, 213, 227, 64, 149, 227, 66, 96, 139, 120, 68, 201, 24, 91, 217, 197, 247, 19, 115, 116, 114, 111, 109, 101, 49, 50, 51, 52, 53, 54, 55, 56, 57, 48, 49, 50, 51, 52, //nolint
			},
			expectedErr: nil,
			expectedHandshake: client.Handshake{
				Pstr:          "BitTorrent protocol",
				ReservedBytes: [8]byte{0, 0, 0, 0, 0, 0, 0, 0},
				InfoHash:      [20]byte{66, 117, 1, 213, 227, 64, 149, 227, 66, 96, 139, 120, 68, 201, 24, 91, 217, 197, 247, 19},
				PeerId:        [20]byte{115, 116, 114, 111, 109, 101, 49, 50, 51, 52, 53, 54, 55, 56, 57, 48, 49, 50, 51, 52},
			},
		},
		{
			name: "unsuccessful deserialize - protocol length is not what expected",
			buffer: []byte{
				23, 66, 105, 116, 84, 111, 114, 114, 101, 110, 116, 32, 112, 114, 111, 116, 111, 99, 111, 108, 0, 0, 0, 0, 0, 0, 0, 0, 66, 117, 1, 213, 227, 64, 149, 227, 66, 96, 139, 120, 68, 201, 24, 91, 217, 197, 247, 19, 115, 116, 114, 111, 109, 101, 49, 50, 51, 52, 53, 54, 55, 56, 57, 48, 49, 50, 51, 52, //nolint
			},
			expectedErr: client.HandshakeSizeError{
				Size: 72,
			},
			expectedHandshake: client.Handshake{},
		},
	}
}
