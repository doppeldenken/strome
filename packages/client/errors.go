package client

import "fmt"

type DifferentInfoHashError struct {
	Size int
}

func (differentInfoHashError DifferentInfoHashError) Error() string {
	return "info hash in received handshake is different from info hash in sent handshake"
}

type EmptyResponseError struct {
	Client string
}

func (emptyResponseError EmptyResponseError) Error() string {
	return fmt.Sprintf(
		"empty response from client '%v'",
		emptyResponseError.Client,
	)
}

type FailedGettingBlockError struct {
	Block  int
	Client string
	Piece  int
}

func (failedGettingBlockError FailedGettingBlockError) Error() string {
	return fmt.Sprintf(
		"failed getting block '%v' of piece '%v' from client '%v'",
		failedGettingBlockError.Block,
		failedGettingBlockError.Piece,
		failedGettingBlockError.Client,
	)
}

type FailedToUnchokeError struct {
	Client string
}

func (failedToUnchokeError FailedToUnchokeError) Error() string {
	return fmt.Sprintf(
		"failed to unchoke client '%v'",
		failedToUnchokeError.Client,
	)
}

type HandshakeSizeError struct {
	Size int
}

func (handshakeSizeError HandshakeSizeError) Error() string {
	return fmt.Sprintf("got handshake size '%v', expected handshakesize '68'", handshakeSizeError.Size)
}

type NotBitfieldResponseError struct {
	Code int
}

func (notBitfieldResponseError NotBitfieldResponseError) Error() string {
	return fmt.Sprintf(
		"expected bitfield response, got '%v'",
		notBitfieldResponseError.Code,
	)
}

type NumberOfPiecesError struct {
	Got      int
	Expected int
}

func (numberOfPiecesError NumberOfPiecesError) Error() string {
	return fmt.Sprintf(
		"got '%v' number of pieces, expected '%v'",
		numberOfPiecesError.Got,
		numberOfPiecesError.Expected,
	)
}

type ParsingBitfieldError struct {
	Reason string
}

func (parsingBitfieldError ParsingBitfieldError) Error() string {
	return fmt.Sprintf(
		"error parsing bitfield: '%v'",
		parsingBitfieldError.Reason,
	)
}

type PeerSentWrongBlockError struct {
	Expected int
	Got      int
}

func (peerSentWrongBlockError PeerSentWrongBlockError) Error() string {
	return fmt.Sprintf(
		"got block '%v', expected block '%v'",
		peerSentWrongBlockError.Got,
		peerSentWrongBlockError.Expected,
	)
}

type PeerSentWrongPieceError struct {
	Expected int
	Got      int
}

func (peerSentWrongPieceError PeerSentWrongPieceError) Error() string {
	return fmt.Sprintf(
		"got block '%v', expected block '%v'",
		peerSentWrongPieceError.Got,
		peerSentWrongPieceError.Expected,
	)
}

type UnexpectedResponseCodeError struct {
	Client   string
	Expected int
	Got      int
}

func (unexpectedResponseCodeError UnexpectedResponseCodeError) Error() string {
	return fmt.Sprintf(
		"unexpected response code from client '%v', got '%v' expected '%v'",
		unexpectedResponseCodeError.Client,
		unexpectedResponseCodeError.Got,
		unexpectedResponseCodeError.Expected,
	)
}
