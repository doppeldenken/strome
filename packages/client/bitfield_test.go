package client_test

import (
	"doppeldenken/strome/packages/client"
	"doppeldenken/strome/packages/utils"
	"errors"
	"testing"
)

func TestBitfield(t *testing.T) {
	t.Parallel()

	testCases := getTestCases()

	for _, testCase := range testCases {
		testCase := testCase

		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()

			bitfield := client.Bitfield{}

			err := bitfield.ParseBitfieldResponse(testCase.response)

			if !errors.Is(err, testCase.expectedErr) {
				t.Errorf(
					"got error '%v', expected error '%v'",
					err,
					testCase.expectedErr,
				)
			}

			if !utils.CompareBoolSlice(bitfield.Pieces, testCase.expectedBitfield) {
				t.Errorf(
					"got bitfield '%v', expected bitfield '%v'",
					bitfield.Pieces,
					testCase.expectedBitfield,
				)
			}
		})
	}
}

type bitfieldTestCase struct {
	name             string
	response         []byte
	expectedErr      error
	expectedBitfield []bool
}

func getTestCases() []bitfieldTestCase {
	return []bitfieldTestCase{
		{
			name: "successful parse response",
			response: []byte{
				5, 255, 255, 244, 5, 10, 0, 0, 37,
			},
			expectedErr: nil,
			expectedBitfield: []bool{
				true, true, true, true, true, true, true, true,
				true, true, true, true, true, true, true, true,
				true, true, true, true, false, true, false, false,
				false, false, false, false, false, true, false, true,
				false, false, false, false, true, false, true, false,
				false, false, false, false, false, false, false, false,
				false, false, false, false, false, false, false, false,
				false, false, true, false, false, true, false, true,
			},
		},
	}
}
