package client_test

// nolint
import (
	"bytes"
	"doppeldenken/strome/packages/client"
	"doppeldenken/strome/packages/mocks"
	"doppeldenken/strome/packages/utils"

	"errors"
	"net"
	"os"
	"strconv"
	"testing"
)

func TestClientDownloadPiece(t *testing.T) {
	t.Parallel()

	testCases := getClientDownloadPieceTestCases()

	for _, testCase := range testCases {
		testCase := testCase
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()

			numberOfReads := 0

			mockConn := getClientDownloadPieceMockConn(testCase)
			mockConn.NumberOfReads = &numberOfReads

			client := client.Client{
				Conn: mockConn,
			}

			_, err := client.DownloadPiece(nil, testCase.mockFileAdapter)

			if !errors.Is(err, testCase.expectedErr) {
				t.Errorf(
					"got error '%v', expected error '%v'",
					err,
					testCase.expectedErr,
				)
			}
		})
	}
}

type mockFileAdapter struct {
	filename   string
	writeError bool
}

func (fileAdapter mockFileAdapter) Name(file *os.File, fullPath bool) (name string, err error) {
	return fileAdapter.filename, nil
}

func (fileAdapter mockFileAdapter) Read(name string) (buffer bytes.Buffer, err error) {
	return buffer, nil
}

func (fileAdapter mockFileAdapter) Write(file *os.File, data []byte, offset int) (n int, err error) {
	if fileAdapter.writeError {
		return 0, os.ErrNotExist
	}

	return 1, nil
}

func getClientDownloadPieceMockConn(testCase clientDownloadPieceTestCase) (mockConn mocks.MockConn) {
	mockConn.Testing = "DownloadPiece"

	if errors.Is(testCase.expectedErr, net.ErrWriteToConnected) {
		mockConn.WriteError = true

		return mockConn
	}

	mockConn.FirstReadError = testCase.firstReadError
	mockConn.SecondReadError = testCase.secondReadError
	mockConn.OnlyZeroBytesError = false

	return mockConn
}

type clientDownloadPieceTestCase struct {
	name            string
	firstReadError  bool
	secondReadError bool
	mockConn        net.Conn
	mockFileAdapter utils.IFileAdapter
	expectedErr     error
}

func getClientDownloadPieceTestCases() []clientDownloadPieceTestCase {
	return []clientDownloadPieceTestCase{
		{
			name: "successful download",
			mockFileAdapter: mockFileAdapter{
				filename:   "0",
				writeError: false,
			},
			expectedErr: nil,
		},
		{
			name: "unsuccessful download - bad file name",
			mockFileAdapter: mockFileAdapter{
				filename:   "abc",
				writeError: false,
			},
			expectedErr: strconv.ErrSyntax,
		},
		{
			name: "unsuccessful download - error requesting piece",
			mockFileAdapter: mockFileAdapter{
				filename:   "0",
				writeError: false,
			},
			expectedErr: net.ErrWriteToConnected,
		},
		{
			name: "unsuccessful download - error receiving response",
			mockFileAdapter: mockFileAdapter{
				filename:   "0",
				writeError: false,
			},
			expectedErr: net.ErrClosed,
		},
		{
			name: "unsuccessful download - error writing to file",
			mockFileAdapter: mockFileAdapter{
				filename:   "0",
				writeError: true,
			},
			expectedErr: os.ErrNotExist,
		},
	}
}
