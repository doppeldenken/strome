package client

import (
	"encoding/binary"
	"fmt"
)

type Message struct {
	Code     uint8
	ClientIp string
	Payload  []byte
}

func (message *Message) Serialize() []byte {
	lengthPrefixSize := 4
	codeSize := 1
	payloadSize := len(message.Payload)

	bufferSize := lengthPrefixSize + codeSize + payloadSize

	messageLength := uint32(codeSize + payloadSize)

	buffer := make([]byte, bufferSize)

	binary.BigEndian.PutUint32(buffer[0:4], messageLength)
	buffer[4] = byte(message.Code)
	copy(buffer[5:], message.Payload)

	fmt.Println(
		fmt.Sprintf(
			"serialized message for client '%v': %v",
			message.ClientIp,
			buffer,
		),
	)

	return buffer
}
