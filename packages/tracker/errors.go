package tracker

import "fmt"

type TrackerProtocolNotAllowedError struct {
	Protocol string
}

func (e TrackerProtocolNotAllowedError) Error() string {
	return fmt.Sprintf(
		"tracker protocol not allowed - '%v'",
		e.Protocol,
	)
}

type FailedGetRequestError struct {
	Message string
	Code    int
}

func (e FailedGetRequestError) Error() string {
	return fmt.Sprintf(
		"get request failed with message '%v' and status code '%v'",
		e.Message,
		e.Code,
	)
}

type NoPortAvailableError struct{}

func (e NoPortAvailableError) Error() string {
	return "no port available (range 6881-6889)"
}

type TorrentNotAvailableError struct {
	Reason string
}

func (e TorrentNotAvailableError) Error() string {
	return fmt.Sprintf(
		"torrent not available at the moment: '%v'",
		e.Reason,
	)
}

type NoPeersAvailableError struct{}

func (e NoPeersAvailableError) Error() string {
	return "no peers available"
}
