package tracker_test

//nolint
import (
	"doppeldenken/strome/packages/metadata"
	"doppeldenken/strome/packages/tracker"
	"errors"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strconv"

	"testing"
)

func TestGet(t *testing.T) {
	t.Parallel()

	testCases := getGetTestCases()

	for _, testCase := range testCases {
		testCase := testCase

		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()

			var gotUrl string

			testServer := getTestServer(&gotUrl, testCase.expectedErr)
			defer testServer.Close()

			testCase = setupTestCase(testCase, testServer.URL)

			response, err := tracker.Get(testCase.torrentMetadata)

			checkResponse(
				t,
				err,
				testCase.expectedErr,
				gotUrl,
				testCase.expectedUrl,
				response,
				testCase.expectedResponse,
			)
		})
	}
}

func getTestServer(gotUrl *string, expectedErr error) (server *httptest.Server) {
	return httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		*gotUrl = r.URL.String()

		if errors.Is(expectedErr, tracker.FailedGetRequestError{
			Message: "internal server error",
			Code:    500,
		}) {
			rw.WriteHeader(500)
			message := "500 - something went wrong"
			rw.Write([]byte(message))

			return
		}

		if errors.Is(expectedErr, tracker.TorrentNotAvailableError{
			Reason: "torrent not found",
		}) {
			message := "d14:failure reason17:torrent not founde"
			rw.Write([]byte(message))

			return
		}

		message := "d8:intervali1234e5:peers6:b�����e"
		rw.Write([]byte(message))
	}))
}

func setupTestCase(testCase getTestCase, testServerUrl string) getTestCase {
	if !errors.Is(testCase.expectedErr, tracker.TrackerProtocolNotAllowedError{
		Protocol: "udp",
	}) {
		testCase.torrentMetadata.Announce = testServerUrl

		sha1Hash, _ := testCase.torrentMetadata.Info.GetTorrentSha1Hash()
		testCase.expectedUrl = "/?" + url.Values{
			"info_hash":  []string{string(sha1Hash[:])},
			"peer_id":    []string{"strome12345678901234"},
			"port":       []string{"6881"},
			"uploaded":   []string{"0"},
			"downloaded": []string{"0"},
			"compact":    []string{"1"},
			"left":       []string{strconv.Itoa(testCase.torrentMetadata.Info.Length)},
		}.Encode()
	}

	return testCase
}

func checkResponse(
	t *testing.T,
	gotErr,
	expectedErr error,
	gotUrl,
	expectedUrl string,
	gotResponse,
	expectedResponse tracker.NotCompactGetRequestResponse,
) {
	if !errors.Is(gotErr, expectedErr) {
		t.Errorf(
			"got error '%v', expected error '%v'",
			gotErr,
			expectedErr,
		)
	}

	if gotUrl != expectedUrl {
		t.Errorf(
			"got url '%v', expected url '%v'",
			gotUrl,
			expectedUrl,
		)
	}

	if !tracker.CompareNotCompactResponses(gotResponse, expectedResponse) {
		t.Errorf(
			"got response '%v', expected response '%v'",
			gotResponse,
			expectedResponse,
		)
	}
}

type getTestCase struct {
	name             string
	torrentMetadata  metadata.TorrentMetadata
	expectedResponse tracker.NotCompactGetRequestResponse
	expectedUrl      string
	expectedErr      error
}

//nolint:funlen
func getGetTestCases() []getTestCase {
	return []getTestCase{
		{
			name: "successful get request - compact",
			torrentMetadata: metadata.TorrentMetadata{
				Announce:     ":6888",
				Comment:      "some comment",
				CreationDate: 123456789,
				Info: metadata.TorrentMetadataInfo{
					Length:      123456789,
					Name:        "torrent",
					PieceLength: 64,
					Pieces:      "qwertyuiopasdfghjklz",
					Files: []metadata.TorrentMetadataInfoFile{
						{
							Length: 0,
							Path:   []string{},
						},
					},
				},
			},
			expectedErr: nil,
			expectedUrl: "/?compact=1&downloaded=0&info_hash=%9B%8A%D6%EF%F21%C2%C4%E9%A0%99%83%F6%3A%EB%D8r%1Fn%F4&left=123456789&peer_id=strome12345678901234&port=6882&uploaded=0", //nolint
			expectedResponse: tracker.NotCompactGetRequestResponse{
				Failure:  "",
				Interval: 1234,
				Peers: []tracker.Peer{
					{
						Id:   [20]byte{},
						Ip:   "98.239.191.189",
						Port: "61375",
					},
				},
			},
		},
		{
			name: "unsuccessful get request", torrentMetadata: metadata.TorrentMetadata{
				Announce:     "http://atracker.com:6888",
				Comment:      "some comment",
				CreationDate: 123456789,
				Info: metadata.TorrentMetadataInfo{
					Length:      123456789,
					Name:        "torrent",
					PieceLength: 64,
					Pieces:      "qwertyuiopasdfghjklz",
					Files: []metadata.TorrentMetadataInfoFile{
						{
							Length: 0,
							Path:   []string{},
						},
					},
				},
			},
			expectedErr: tracker.FailedGetRequestError{
				Message: "internal server error",
				Code:    500,
			},
			expectedUrl:      "/?compact=1&downloaded=0&info_hash=%07%9E%8A%3Af%C3H%8B%3B%CFp%24%BC%2C%A5%7F%0F3%CBs&left=123456789&peer_id=strome12345678901234&port=6882&uploaded=0", //nolint
			expectedResponse: tracker.NotCompactGetRequestResponse{},
		},
		{
			name: "unsuccessful get request - protocol not allowed", torrentMetadata: metadata.TorrentMetadata{
				Announce:     "udp://atracker.com:6888",
				Comment:      "some comment",
				CreationDate: 123456789,
				Info: metadata.TorrentMetadataInfo{
					Length:      123456789,
					Name:        "torrent",
					PieceLength: 64,
					Pieces:      "qwertyuiopasdfghjklz",
					Files: []metadata.TorrentMetadataInfoFile{
						{
							Length: 0,
							Path:   []string{},
						},
					},
				},
			},
			expectedErr: tracker.TrackerProtocolNotAllowedError{
				Protocol: "udp",
			},
			expectedUrl:      "",
			expectedResponse: tracker.NotCompactGetRequestResponse{},
		},
		{
			name: "unsuccessful get request - tracker responded with a failure key", torrentMetadata: metadata.TorrentMetadata{
				Announce:     "http://atracker.com:6888",
				Comment:      "some comment",
				CreationDate: 123456789,
				Info: metadata.TorrentMetadataInfo{
					Length:      123456789,
					Name:        "torrent",
					PieceLength: 64,
					Pieces:      "qwertyuiopasdfghjklz",
					Files: []metadata.TorrentMetadataInfoFile{
						{
							Length: 0,
							Path:   []string{},
						},
					},
				},
			},
			expectedErr: tracker.TorrentNotAvailableError{
				Reason: "torrent not found",
			},
			expectedUrl: "/?compact=1&downloaded=0&info_hash=%9B%8A%D6%EF%F21%C2%C4%E9%A0%99%83%F6%3A%EB%D8r%1Fn%F4&left=123456789&peer_id=strome12345678901234&port=6882&uploaded=0", //nolint
			expectedResponse: tracker.NotCompactGetRequestResponse{
				Failure:  "torrent not found",
				Interval: 0,
				Peers:    []tracker.Peer{},
			},
		},
	}
}
