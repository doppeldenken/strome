package tracker

//nolint
import (
	"doppeldenken/strome/packages/metadata"
	"encoding/binary"
	"errors"
	"time"

	"fmt"
	"io"
	"net"
	"strconv"
	"strings"

	"net/http"
	"net/url"

	"github.com/jackpal/bencode-go"
)

func Get(torrentMetadata metadata.TorrentMetadata) (getRequestResponse NotCompactGetRequestResponse, err error) {
	if err = checkForHttp(torrentMetadata.Announce); err != nil {
		return getRequestResponse, err
	}

	response, err := trackerRequest(torrentMetadata, compactResponse)
	if err != nil {
		return getRequestResponse, err
	}
	defer response.Body.Close()

	parsedCompactResponse, err := parseCompactResponse(response)
	if err != nil {
		if errors.Is(err, NoPeersAvailableError{}) {
			response, err = trackerRequest(torrentMetadata, notCompactResponse)
			if err != nil {
				return getRequestResponse, err
			}
			defer response.Body.Close()

			return parseNotCompactResponse(response)
		} else {
			getRequestResponse.Failure = parsedCompactResponse.Failure

			return getRequestResponse, err
		}
	}

	peersList := transformPeersStringToPeersList(parsedCompactResponse.Peers)

	getRequestResponse.Interval = parsedCompactResponse.Interval
	getRequestResponse.Peers = peersList

	return getRequestResponse, nil
}

func checkForHttp(trackerUrl string) error {
	scheme := strings.Split(trackerUrl, "://")
	if scheme[0] != "http" && scheme[0] != "https" {
		return TrackerProtocolNotAllowedError{
			Protocol: scheme[0],
		}
	}

	return nil
}

func trackerRequest(
	torrentMetadata metadata.TorrentMetadata,
	compactResponse string,
) (
	response *http.Response,
	err error,
) {
	queryParams, err := getQueryParams(torrentMetadata, compactResponse)
	if err != nil {
		return nil, err
	}

	request, err := http.NewRequest("GET", torrentMetadata.Announce, nil)
	if err != nil {
		return nil, err
	}

	request.Close = true

	request.URL.RawQuery = queryParams.Encode()

	client := http.Client{Timeout: trackerConnectionTimeout * time.Second}

	return client.Do(request)
}

func getQueryParams(torrentMetadata metadata.TorrentMetadata, compact string) (queryParams url.Values, err error) {
	sha1HashBytes, err := torrentMetadata.Info.GetTorrentSha1Hash()
	if err != nil {
		return queryParams, err
	}

	port, err := getAvailablePort()
	if err != nil {
		return queryParams, err
	}

	values := url.Values{
		"info_hash":  []string{string(sha1HashBytes[:])},
		"peer_id":    []string{stromePeerId},
		"port":       []string{port},
		"uploaded":   []string{"0"},
		"downloaded": []string{"0"},
		"compact":    []string{compact},
		"left":       []string{strconv.Itoa(torrentMetadata.Info.Length)},
	}

	return values, nil
}

func getAvailablePort() (availablePort string, err error) {
	ports := []string{
		"6881",
		"6882",
		"6883",
		"6884",
		"6885",
		"6886",
		"6887",
		"6888",
		"6889",
	}

	for _, port := range ports {
		listen, err := net.Listen("tcp", ":"+port)
		if err == nil {
			err = listen.Close()
			if err == nil {
				return port, nil
			}
		}
	}

	return availablePort, NoPortAvailableError{}
}

func parseCompactResponse(response *http.Response) (compactGetRequestResponse CompactGetRequestResponse, err error) {
	if err = checkResponseStatus(response); err != nil {
		return compactGetRequestResponse, err
	}

	err = bencode.Unmarshal(response.Body, &compactGetRequestResponse)
	if err != nil && err != io.EOF {
		return compactGetRequestResponse, err
	}

	if compactGetRequestResponse.Failure != "" {
		return compactGetRequestResponse, TorrentNotAvailableError{
			Reason: compactGetRequestResponse.Failure,
		}
	}

	if compactGetRequestResponse.Peers == "" {
		return compactGetRequestResponse, NoPeersAvailableError{}
	}

	return compactGetRequestResponse, nil
}

func parseNotCompactResponse(response *http.Response) (
	notCompactGetRequestResponse NotCompactGetRequestResponse,
	err error,
) {
	if err = checkResponseStatus(response); err != nil {
		return notCompactGetRequestResponse, err
	}

	err = bencode.Unmarshal(response.Body, &notCompactGetRequestResponse)
	if err != nil && err != io.EOF {
		return notCompactGetRequestResponse, err
	}

	if notCompactGetRequestResponse.Failure != "" {
		return notCompactGetRequestResponse, TorrentNotAvailableError{
			Reason: notCompactGetRequestResponse.Failure,
		}
	}

	if ComparePeers(notCompactGetRequestResponse.Peers, []Peer{}) {
		return notCompactGetRequestResponse, NoPeersAvailableError{}
	}

	return notCompactGetRequestResponse, nil
}

func checkResponseStatus(response *http.Response) error {
	if response.StatusCode != http.StatusOK {
		body, err := readBody(response)
		if err != nil {
			return err
		}

		return FailedGetRequestError{
			Message: string(body),
			Code:    response.StatusCode,
		}
	}

	return nil
}

func readBody(response *http.Response) (body []byte, err error) {
	n := 1
	bytesToRead := 16

	for n > 0 {
		buf := make([]byte, bytesToRead)
		n, err = response.Body.Read(buf)

		if err != nil {
			return body, FailedGetRequestError{
				Message: fmt.Sprintf("internal server error"),
				Code:    http.StatusInternalServerError,
			}
		}

		body = append(body, buf...)
	}

	return body, nil
}

func transformPeersStringToPeersList(strList string) (peers []Peer) {
	stepSize := 6
	end := stepSize

	peersListAsBytes := []byte(strList)

	for start := 0; start < len(peersListAsBytes); start += stepSize {
		peer := peersListAsBytes[start:end]

		peers = append(
			peers,
			Peer{
				Id:   PeerId{},
				Ip:   fmt.Sprintf("%v", net.IP(peer[0:4])),
				Port: fmt.Sprintf("%v", binary.BigEndian.Uint16(peer[4:6])),
			},
		)

		end += stepSize
	}

	return peers
}
