package download_test

// //nolint
// import (
// 	"doppeldenken/strome/packages/client"
// 	"doppeldenken/strome/packages/download"
// 	"doppeldenken/strome/packages/files"
// 	"doppeldenken/strome/packages/utils"
// 	"net"
// 	"os"
// 	"time"

// 	"testing"
// )

// func TestRun(t *testing.T) {
// 	t.Parallel()

// 	testCases := getTestCases()

// 	for _, testCase := range testCases {
// 		testCase := testCase

// 		t.Run(testCase.name, func(t *testing.T) {
// 			t.Parallel()

// 			availablePeers := getAvailablePeers(testCase)

// 			mockDownloadFileAdapter := mockDownloadFileAdapter{}

// 			download.Run(
// 				availablePeers,
// 				testCase.numberOfPieces,
// 				mockDownloadFileAdapter,
// 			)

// 			// for i, err := range errs {
// 			// 	if len(errs) != len(testCase.expectedErrs) || !errors.Is(err, testCase.expectedErrs[i]) {
// 			// 		t.Errorf(
// 			// 			"got '%v' errors, expected '%v' errors",
// 			// 			len(errs),
// 			// 			len(testCase.expectedErrs),
// 			// 		)
// 			// 	}
// 			// }
// 		})
// 	}
// }

// type mockClient struct {
// 	sendInterestedError  bool
// 	receiveResponseError bool
// 	notBitfieldCodeError bool
// 	numberOfPiecesError  bool
// }

// func (client *mockClient) GetClientTargetIp() string {
// 	return ""
// }

// func (client *mockClient) RemoveDeadline() error {
// 	return nil
// }

// func (client *mockClient) SetDeadline(seconds time.Duration) error {
// 	return nil
// }

// func (client *mockClient) Close() error {
// 	return nil
// }

// func (
// 	client *mockClient,
// ) DownloadPiece(file *os.File, fileAdapter utils.IFileAdapter) (piece []byte, err error) {
// 	return []byte{}, nil
// }

// func (client *mockClient) FirstUnchoke() error {
// 	return nil
// }

// func (client *mockClient) GetPieceFromPeer(pieceNumber int) (piece []byte, err error) {
// 	return []byte{}, nil
// }

// func (client *mockClient) ReceiveResponse(expectedResponse int) (response []byte, err error) {
// 	if client.receiveResponseError {
// 		return []byte{}, net.ErrClosed
// 	}

// 	if client.notBitfieldCodeError {
// 		return []byte{4, 1, 2, 3}, nil
// 	}

// 	if client.numberOfPiecesError {
// 		return []byte{5, 0}, nil
// 	}

// 	return []byte{
// 		5, 255, 255,
// 	}, nil
// }

// func (client *mockClient) RequestBlock(pieceNumber, blockNumber int) error {
// 	return nil
// }

// func (client *mockClient) RequestPiece(pieceNumber int) error {
// 	return nil
// }

// func (client *mockClient) SendHave(pieceNumber int) error {
// 	return nil
// }

// func (client *mockClient) SendInterested() error {
// 	if client.sendInterestedError {
// 		return net.ErrClosed
// 	}

// 	return nil
// }

// func (client *mockClient) Unchoke() (unchoked bool, err error) {
// 	return true, nil
// }

// type mockDownloadFileAdapter struct{}

// func (
// 	downloadFileAdapter mockDownloadFileAdapter,
// ) CheckIfAllPiecesAreDownloaded() (allPiecesDownloaded bool, err error) {
// 	return false, nil
// }

// func (
// 	downloadFileAdapter mockDownloadFileAdapter,
// ) CheckPieceIntegrity(piece []byte, pieceNumber int) error {
// 	return nil
// }

// func (
// 	downloadFileAdapter mockDownloadFileAdapter,
// ) CleanIncompletePieces() (cleanedPieces []string, err error) {
// 	return []string{}, nil
// }

// func (
// 	downloadFileAdapter mockDownloadFileAdapter,
// ) CreatePieceFile(name string) (
// 	file *os.File,
// 	err error,
// ) {
// 	return nil, nil
// }

// func (downloadFileAdapter mockDownloadFileAdapter) DeletePieceFile(name string) error {
// 	return nil
// }

// func (
// 	downloadFileAdapter mockDownloadFileAdapter,
// ) GetDownloadedPieces() (
// 	downloadedPieces files.DownloadedPieces,
// 	err error,
// ) {
// 	return nil, nil
// }

// func (
// 	downloadFileAdapter mockDownloadFileAdapter,
// ) GetPiecesToDownload() (
// 	piecesToDownload []int,
// 	err error,
// ) {
// 	return []int{}, nil
// }

// func (downloadFileAdapter mockDownloadFileAdapter) UpdatePiecesFile(pieceN int) error {
// 	return nil
// }

// type downloadTestCase struct {
// 	name                     string
// 	availablePeers           []mockClient
// 	numberOfPieces           int
// 	expectedErrs             []error
// 	expectedDownloadedPieces [][]bool
// }

// func getAvailablePeers(testCase downloadTestCase) []*client.IClient {
// 	var availablePeers []client.IClient

// 	var availablePeersPointers []*client.IClient

// 	for i := 0; i < len(testCase.availablePeers); i++ {
// 		mockClient := &testCase.availablePeers[i]
// 		availablePeers = append(availablePeers, mockClient)
// 	}

// 	for i := 0; i < len(availablePeers); i++ {
// 		availablePeersPointers = append(availablePeersPointers, &availablePeers[i])
// 	}

// 	return availablePeersPointers
// }

// //nolint:funlen
// func getTestCases() []downloadTestCase {
// 	return []downloadTestCase{
// 		{
// 			name: "successful download",
// 			availablePeers: []mockClient{
// 				{
// 					sendInterestedError:  false,
// 					receiveResponseError: false,
// 					notBitfieldCodeError: false,
// 					numberOfPiecesError:  false,
// 				},
// 			},
// 			numberOfPieces: 16,
// 			expectedErrs:   []error{},
// 			expectedDownloadedPieces: [][]bool{
// 				{
// 					true,
// 				},
// 			},
// 		},
// 		{
// 			name: "unsuccessful download - send interested error",
// 			availablePeers: []mockClient{
// 				{
// 					sendInterestedError:  true,
// 					receiveResponseError: false,
// 					notBitfieldCodeError: false,
// 					numberOfPiecesError:  false,
// 				},
// 			},
// 			numberOfPieces: 0,
// 			expectedErrs: []error{
// 				net.ErrClosed,
// 			},
// 			expectedDownloadedPieces: [][]bool{},
// 		},
// 		{
// 			name: "unsuccessful download - receive response error",
// 			availablePeers: []mockClient{
// 				{
// 					sendInterestedError:  false,
// 					receiveResponseError: true,
// 					notBitfieldCodeError: false,
// 					numberOfPiecesError:  false,
// 				},
// 			},
// 			numberOfPieces: 0,
// 			expectedErrs: []error{
// 				net.ErrClosed,
// 			},
// 			expectedDownloadedPieces: [][]bool{},
// 		},
// 		{
// 			name: "unsuccessful download - parsing bitfield - not bitfield response code",
// 			availablePeers: []mockClient{
// 				{
// 					sendInterestedError:  false,
// 					receiveResponseError: false,
// 					notBitfieldCodeError: true,
// 					numberOfPiecesError:  false,
// 				},
// 			},
// 			numberOfPieces: 0,
// 			expectedErrs: []error{
// 				client.NotBitfieldResponseError{
// 					Code: 4,
// 				},
// 			},
// 			expectedDownloadedPieces: [][]bool{},
// 		},
// 		{
// 			name: "unsuccessful download - parsing bitfield - error parsing bitfield",
// 			availablePeers: []mockClient{
// 				{
// 					sendInterestedError:  false,
// 					receiveResponseError: false,
// 					notBitfieldCodeError: false,
// 					numberOfPiecesError:  true,
// 				},
// 			},
// 			numberOfPieces: 1,
// 			expectedErrs: []error{
// 				client.NumberOfPiecesError{
// 					Got:      8,
// 					Expected: 1,
// 				},
// 			},
// 			expectedDownloadedPieces: [][]bool{},
// 		},
// 	}
// }
