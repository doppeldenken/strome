package download

type EndDownloadInfo struct {
	ClientIp        string
	PieceDownloaded bool
	PieceNumber     int
}
