package download

//nolint
import (
	"crypto/sha1"

	"doppeldenken/strome/packages/files"
	"doppeldenken/strome/packages/utils"

	"fmt"
	"os"
)

type IDownloadFileAdapter interface {
	CheckPieceIntegrity(piece []byte, expectedPieceHash [20]byte) error
	CleanIncompletePieces() (cleanedPieces []string, err error)
	CreatePieceFile(name string) (file *os.File, err error)
	DeletePieceFile(name string) error
	GetDownloadedPieces() (downloadedPieces files.DownloadedPieces, err error)
	GetPiecesToDownload() (piecesToDownload []int, err error)
}

type DownloadFileAdapter struct {
	FileAdapter       utils.IFileAdapter
	JsonAdapter       utils.IJsonAdapter
	NumberOfPieces    int
	OsAdapter         utils.IOsAdapter
	PieceLength       int
	TorrentTmpRootDir string
}

func (
	downloadFileAdapter DownloadFileAdapter,
) CheckPieceIntegrity(piece []byte, expectedPieceHash [20]byte) error {
	downloadedPieceHash := sha1.Sum(piece)

	if !utils.Compare20ByteArray(downloadedPieceHash, expectedPieceHash) {
		return DownloadedPieceWrongHashError{
			Got:      downloadedPieceHash,
			Expected: expectedPieceHash,
		}
	}

	return nil
}

func (
	downloadFileAdapter DownloadFileAdapter,
) CleanIncompletePieces() (cleanedPieces []string, err error) {
	entries, err := downloadFileAdapter.OsAdapter.ReadDir(
		fmt.Sprintf(
			"%v/.pieces",
			downloadFileAdapter.TorrentTmpRootDir,
		),
	)
	if err != nil {
		return []string{}, err
	}

	for _, entry := range entries {
		fileInfo, err := entry.Info()
		if err != nil {
			return []string{}, err
		}

		if fileInfo.Size() != int64(downloadFileAdapter.PieceLength) {
			filename := fileInfo.Name()

			err := downloadFileAdapter.OsAdapter.Remove(
				fmt.Sprintf(
					"%v/.pieces/%v",
					downloadFileAdapter.TorrentTmpRootDir,
					filename,
				),
			)
			if err != nil {
				return []string{}, err
			}

			cleanedPieces = append(cleanedPieces, filename)
		}
	}

	return cleanedPieces, nil
}

func (
	downloadFileAdapter DownloadFileAdapter,
) CreatePieceFile(name string) (
	file *os.File,
	err error,
) {
	entries, err := downloadFileAdapter.OsAdapter.ReadDir(
		fmt.Sprintf(
			"%v/%v",
			downloadFileAdapter.TorrentTmpRootDir,
			".pieces",
		),
	)
	if err != nil {
		return nil, err
	}

	for _, entry := range entries {
		if entry.Name() == name {
			return nil, PieceFileAlreadyExistsError{
				Name: name,
			}
		}
	}

	fullName := fmt.Sprintf(
		"%v/.pieces/%v",
		downloadFileAdapter.TorrentTmpRootDir,
		name,
	)

	return downloadFileAdapter.OsAdapter.Create(fullName, false)
}

func (downloadFileAdapter DownloadFileAdapter) DeletePieceFile(name string) error {
	return downloadFileAdapter.OsAdapter.Remove(
		fmt.Sprintf(
			"%v/.pieces/%v",
			downloadFileAdapter.TorrentTmpRootDir,
			name,
		),
	)
}

func (
	downloadFileAdapter DownloadFileAdapter,
) GetDownloadedPieces() (
	downloadedPieces files.DownloadedPieces,
	err error,
) {
	data, err := downloadFileAdapter.OsAdapter.Read(
		fmt.Sprintf(
			"%v/%v",
			downloadFileAdapter.TorrentTmpRootDir,
			files.PiecesFileName,
		),
	)
	if err != nil {
		return nil, err
	}

	err = downloadFileAdapter.JsonAdapter.Unmarshal(data, &downloadedPieces)
	if err != nil {
		return nil, err
	}

	return downloadedPieces, nil
}

func (
	downloadFileAdapter DownloadFileAdapter,
) GetPiecesToDownload() (
	piecesToDownload []int,
	err error,
) {
	downloadedPieces, err := downloadFileAdapter.GetDownloadedPieces()

	for key, value := range downloadedPieces {
		if !value {
			piecesToDownload = append(piecesToDownload, key)
		}
	}

	return piecesToDownload, nil
}
