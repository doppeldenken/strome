package download

//nolint
import (
	clientPackage "doppeldenken/strome/packages/client"
	"doppeldenken/strome/packages/utils"

	"fmt"
)

func Run(
	availablePeerClient *clientPackage.Client,
	pieceToDownload int,
	downloadFileAdapter DownloadFileAdapter,
	pieceHash [20]byte,
) bool {
	client := *availablePeerClient
	defer client.Close()
	defer client.RemoveDeadline()

	clientIp := client.GetClientTargetIp()

	client.SetDeadline(3)

	if err := client.Unchoke(); err != nil {
		fmt.Println(
			fmt.Sprintf(
				"error sending unchoke message to client '%v': %v",
				clientIp,
				err,
			),
		)

		return false
	}

	fmt.Println(
		fmt.Sprintf(
			"successfully sent unchoke to client '%v",
			clientIp,
		),
	)

	response, err := client.ReceiveResponse(clientPackage.BitfieldCode)
	if err != nil {
		fmt.Println(
			fmt.Sprintf(
				"error getting response from client '%v' to unchoke request: %v",
				clientIp,
				err,
			),
		)

		return false
	}

	bitfield, err := getBitfield(response, downloadFileAdapter.NumberOfPieces)
	if err != nil {
		fmt.Println(
			fmt.Sprintf(
				"error parsing bitfield from client '%v': %v",
				clientIp,
				err,
			),
		)

		return false
	}

	fmt.Println(
		fmt.Sprintf(
			"successfully got bitfield from client '%v'",
			clientIp,
		),
	)

	if !bitfield.Pieces[pieceToDownload] {
		fmt.Println(
			fmt.Sprintf(
				"client '%v' does not have piece '%v'; aborting piece download",
				clientIp,
				pieceToDownload,
			),
		)

		return false
	}

	file, err := downloadFileAdapter.CreatePieceFile(fmt.Sprintf("%v", pieceToDownload))
	if err != nil {
		fmt.Println(
			fmt.Sprintf(
				"error creating piece file '%v' for client '%v': %v",
				pieceToDownload,
				clientIp,
				err,
			),
		)

		return false
	}

	defer file.Close()

	fmt.Println(
		fmt.Sprintf(
			"piece file '%v' created successfully for client '%v'",
			file.Name(),
			clientIp,
		),
	)

	fileAdapter := utils.FileAdapter{}

	piece, err := client.DownloadPiece(file, fileAdapter)
	if err != nil {
		fmt.Println(
			fmt.Sprintf(
				"error downloading piece file '%v' for client '%v': %v",
				pieceToDownload,
				clientIp,
				err,
			),
		)

		return false
	}

	fmt.Println(
		fmt.Sprintf(
			"piece file '%v' downloaded successfully for client '%v'",
			file.Name(),
			clientIp,
		),
	)

	err = downloadFileAdapter.CheckPieceIntegrity(piece, pieceHash)
	if err != nil {
		fmt.Println(
			fmt.Sprintf(
				"error checkinng piece '%v' integrity for client '%v': %v",
				pieceToDownload,
				clientIp,
				err,
			),
		)

		return false
	}

	fmt.Println(
		fmt.Sprintf(
			"piece file '%v' integrity check passed for client '%v'",
			pieceToDownload,
			clientIp,
		),
	)

	_, err = fileAdapter.Write(file, piece, -1)
	if err != nil {
		fmt.Println(
			fmt.Sprintf(
				"error writing piece '%v' to file for client '%v': %v",
				pieceToDownload,
				clientIp,
				err,
			),
		)

		return false
	}

	fmt.Println(
		fmt.Sprintf(
			"successfully written piece '%v' to file from client '%v'",
			pieceToDownload,
			clientIp,
		),
	)

	client.SetDeadline(3)

	err = client.SendHave(pieceToDownload)
	if err != nil {
		fmt.Println(
			fmt.Sprintf(
				"error sending have message to client '%v': %v",
				clientIp,
				err,
			),
		)
	}

	_, err = client.ReceiveResponse(clientPackage.HaveCode)
	if err != nil {
		fmt.Println(
			fmt.Sprintf(
				"error sending have message to client '%v': %v",
				clientIp,
				err,
			),
		)
	}

	fmt.Println(
		fmt.Sprintf(
			"DOWNLOADED PIECE '%v' FROM CLIENT '%v'",
			pieceToDownload,
			clientIp,
		),
	)

	return true
}

func getBitfield(response []byte, numberOfPieces int) (bitfield clientPackage.Bitfield, err error) {
	if response[0] == clientPackage.BitfieldCode {
		if err := bitfield.ParseBitfieldResponse(response); err != nil {
			return clientPackage.Bitfield{}, err
		}
	} else {
		return clientPackage.Bitfield{}, clientPackage.NotBitfieldResponseError{
			Code: int(response[0]),
		}
	}

	if len(bitfield.Pieces) != numberOfPieces {
		return clientPackage.Bitfield{}, clientPackage.NumberOfPiecesError{
			Got:      len(bitfield.Pieces),
			Expected: numberOfPieces,
		}
	}

	return bitfield, nil
}
