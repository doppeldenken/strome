package download

import "fmt"

type AllPiecesDownloadedError struct{}

func (allPiecesDownloadedError AllPiecesDownloadedError) Error() string {
	return "all pieces downloaded"
}

type DownloadedPieceWrongHashError struct {
	Expected [20]byte
	Got      [20]byte
}

func (downloadedPieceWrongHashError DownloadedPieceWrongHashError) Error() string {
	return fmt.Sprintf(
		"got downloaded piece hash '%v', expected '%v'",
		downloadedPieceWrongHashError.Got,
		downloadedPieceWrongHashError.Expected,
	)
}

type NoAvailablePieceError struct{}

func (noAvailablePieceError NoAvailablePieceError) Error() string {
	return "no available piece"
}

type NotBitfieldResponse struct{}

func (notBitfieldResponse NotBitfieldResponse) Error() string {
	return "not bitfield response"
}

type PieceFileAlreadyExistsError struct {
	Name string
}

func (pieceFileAlreadtExistsError PieceFileAlreadyExistsError) Error() string {
	return fmt.Sprintf(
		"piece file '%v' already exists",
		pieceFileAlreadtExistsError.Name,
	)
}
