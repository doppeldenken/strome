package download_test

//nolint
import (
	"doppeldenken/strome/packages/download"
	"doppeldenken/strome/packages/mocks"
	"doppeldenken/strome/packages/utils"
	"io/fs"

	"errors"
	"testing"
)

func TestCleanIncompletePieces(t *testing.T) {
	// t.Parallel()

	testCases := getCleanIncompletePiecesTestCases()

	for _, testCase := range testCases {
		testCase := testCase

		downloadFileAdapter := download.DownloadFileAdapter{
			OsAdapter:   testCase.mockOSAdapter,
			PieceLength: testCase.pieceLength,
		}

		cleanedPieces, err := downloadFileAdapter.CleanIncompletePieces()

		t.Run(testCase.name, func(t *testing.T) {
			// t.Parallel()

			if !errors.Is(err, testCase.expectedErr) {
				t.Errorf(
					"got error '%v', expected error '%v'",
					err,
					testCase.expectedErr,
				)
			}

			if !utils.CompareStringSlices(cleanedPieces, testCase.expectedCleanedPieces) {
				t.Errorf(
					"got cleanedPieces '%v', expected cleanedPieces '%v'",
					cleanedPieces,
					testCase.expectedCleanedPieces,
				)
			}
		})
	}
}

type cleanIncompletePiecesTestCase struct {
	name                  string
	mockOSAdapter         utils.IOsAdapter
	pieceLength           int
	expectedCleanedPieces []string
	expectedErr           error
}

func getCleanIncompletePiecesTestCases() []cleanIncompletePiecesTestCase {
	return []cleanIncompletePiecesTestCase{
		{
			name: "successfully cleans incomplete pieces - no cleaned pieces",
			mockOSAdapter: mocks.MockOsAdapter{
				ReadDirError: false,
				ReadDirFiles: []mocks.DirEntryInfo{
					{
						FileSize:             10,
						ErrorGettingFileInfo: false,
					},
					{
						FileSize:             10,
						ErrorGettingFileInfo: false,
					},
				},
				RemovingFileError: false,
			},
			pieceLength:           10,
			expectedCleanedPieces: []string{},
			expectedErr:           nil,
		},
		{
			name: "successfully cleans incomplete pieces - cleaned 1 piece",
			mockOSAdapter: mocks.MockOsAdapter{
				ReadDirError: false,
				ReadDirFiles: []mocks.DirEntryInfo{
					{
						FileSize:             10,
						ErrorGettingFileInfo: false,
					},
					{
						FileSize:             5,
						ErrorGettingFileInfo: false,
					},
				},
				RemovingFileError: false,
			},
			pieceLength:           10,
			expectedCleanedPieces: []string{"test"},
			expectedErr:           nil,
		},
		{
			name: "fails cleaning incomplete pieces - fails reading dir",
			mockOSAdapter: mocks.MockOsAdapter{
				ReadDirError: true,
				ReadDirFiles: []mocks.DirEntryInfo{
					{
						FileSize:             10,
						ErrorGettingFileInfo: false,
					},
					{
						FileSize:             10,
						ErrorGettingFileInfo: false,
					},
				},
				RemovingFileError: false,
			},
			pieceLength:           10,
			expectedCleanedPieces: []string{},
			expectedErr:           fs.ErrInvalid,
		},
		{
			name: "fails cleaning incomplete pieces - fails getting file info",
			mockOSAdapter: mocks.MockOsAdapter{
				ReadDirError: false,
				ReadDirFiles: []mocks.DirEntryInfo{
					{
						FileSize:             10,
						ErrorGettingFileInfo: false,
					},
					{
						FileSize:             10,
						ErrorGettingFileInfo: true,
					},
				},
				RemovingFileError: false,
			},
			pieceLength:           10,
			expectedCleanedPieces: []string{},
			expectedErr:           fs.ErrNotExist,
		},
		{
			name: "fails cleaning incomplete pieces - error removing incomplete piece",
			mockOSAdapter: mocks.MockOsAdapter{
				ReadDirError: false,
				ReadDirFiles: []mocks.DirEntryInfo{
					{
						FileSize:             10,
						ErrorGettingFileInfo: false,
					},
					{
						FileSize:             5,
						ErrorGettingFileInfo: false,
					},
				},
				RemovingFileError: true,
			},
			pieceLength:           10,
			expectedCleanedPieces: []string{},
			expectedErr:           fs.ErrPermission,
		},
	}
}
