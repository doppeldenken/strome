package download_test

//nolint
import (
	"doppeldenken/strome/packages/download"
	"doppeldenken/strome/packages/files"
	"doppeldenken/strome/packages/utils"

	"encoding/json"
	"errors"
	"io/fs"
	"os"
	"testing"
)

func TestDownloadFileAdapterGetDownloadedPieces(t *testing.T) {
	t.Parallel()

	testCases := getDownloadFileAdapterGetDownloadedPiecesTestCases()

	for _, testCase := range testCases {
		testCase := testCase

		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()

			downloadFileAdapter := download.DownloadFileAdapter{
				FileAdapter:       utils.FileAdapter{},
				JsonAdapter:       testCase.mockGetDownloadedPiecesJsonAdapter,
				OsAdapter:         testCase.mockGetDownloadedPiecesOsAdapter,
				TorrentTmpRootDir: "/tmp/.strome",
			}

			downloadedPieces, err := downloadFileAdapter.GetDownloadedPieces()

			if !errors.Is(err, testCase.expectedErr) {
				t.Errorf(
					"got error '%v', expected error '%v'",
					err,
					testCase.expectedErr,
				)
			}

			if !files.CompareDownloadedPieces(downloadedPieces, testCase.expectedDownloadedPieces) {
				t.Errorf(
					"got downloaded pieces '%v', expected downloaded pieces '%v'",
					downloadedPieces,
					testCase.expectedDownloadedPieces,
				)
			}
		})
	}
}

type mockGetDownloadedPiecesOsAdapter struct {
	errorReadingFile bool
}

type mockGetDownloadedPiecesJsonAdapter struct {
	errorUnmarshallFile bool
}

func (osAdapter mockGetDownloadedPiecesOsAdapter) Create(name string, truncate bool) (*os.File, error) {
	return nil, nil
}

func (osAdapter mockGetDownloadedPiecesOsAdapter) Executable() (string, error) {
	return os.Executable()
}

func (jsonAdapter mockGetDownloadedPiecesJsonAdapter) Marshal(v interface{}) (data []byte, err error) {
	return []byte{}, nil
}

func (
	jsonAdapter mockGetDownloadedPiecesJsonAdapter,
) MarshalIndent(v interface{}, prefix string, indent string) (
	[]byte,
	error,
) {
	return []byte{}, nil
}

func (jsonAdapter mockGetDownloadedPiecesJsonAdapter) Unmarshal(data []byte, v interface{}) error {
	return json.Unmarshal(data, v)
}

func (osAdapter mockGetDownloadedPiecesOsAdapter) Read(name string) ([]byte, error) {
	if osAdapter.errorReadingFile {
		return []byte{}, os.ErrPermission
	}

	return []byte{
		123, 10, 32, 32, 34, 48, 34, 58, 32, 116, 114, 117, 101, 44, 10, 32, 32, 34, 49, 34, 58, 32, 102, 97, 108, 115, 101, 44, 10, 32, 32, 34, 50, 34, 58, 32, 116, 114, 117, 101, 44, 10, 32, 32, 34, 51, 34, 58, 32, 102, 97, 108, 115, 101, 10, 125, //nolint
	}, nil
}

func (osAdapter mockGetDownloadedPiecesOsAdapter) ReadDir(name string) (entries []fs.DirEntry, err error) {
	return []fs.DirEntry{}, nil
}

func (osAdapter mockGetDownloadedPiecesOsAdapter) Remove(name string) error {
	return nil
}

func (osAdapter mockGetDownloadedPiecesOsAdapter) Stat(name string) (fs.FileInfo, error) {
	return nil, nil
}

type downloadFileAdapterGetDownloadedPiecesFileTestCase struct {
	name                               string
	mockGetDownloadedPiecesOsAdapter   mockGetDownloadedPiecesOsAdapter
	mockGetDownloadedPiecesJsonAdapter mockGetDownloadedPiecesJsonAdapter
	expectedErr                        error
	expectedDownloadedPieces           files.DownloadedPieces
}

// TODO: add test if json unmarshall fails
func getDownloadFileAdapterGetDownloadedPiecesTestCases() []downloadFileAdapterGetDownloadedPiecesFileTestCase {
	return []downloadFileAdapterGetDownloadedPiecesFileTestCase{
		{
			name: "successfully get downloaded pieces",
			mockGetDownloadedPiecesOsAdapter: mockGetDownloadedPiecesOsAdapter{
				errorReadingFile: false,
			},
			expectedErr: nil,
			expectedDownloadedPieces: files.DownloadedPieces{
				0: true,
				1: false,
				2: true,
				3: false,
			},
		},
		{
			name: "get downloaded pieces file fails - error reading file",
			mockGetDownloadedPiecesOsAdapter: mockGetDownloadedPiecesOsAdapter{
				errorReadingFile: true,
			},
			expectedErr:              os.ErrPermission,
			expectedDownloadedPieces: files.DownloadedPieces{},
		},
	}
}
